package com.caloriecounter.Fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.caloriecounter.Dialogs.SimpleInfoDialog;
import com.caloriecounter.Objects.Food;
import com.caloriecounter.R;
import com.caloriecounter.Utils.DietDataManager;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

/**
 * A simple {@link Fragment} subclass.
 */
public class FoodListFragment extends Fragment {

    private ProgressDialog progressDialog;
    private ListView listView;

    public FoodListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_food_list, container, false);
        Bundle args = getArguments();
        String category = args.getString("category");
        listView = (ListView) view.findViewById(R.id.foodListFragmentListView);
        AsyncTask updateFoodList = new UpdateFoodList();
        updateFoodList.execute(new String[]{category});
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading Food Items ......");
        progressDialog.setTitle("Loading");
        progressDialog.show();
        return view;
    }

    private class UpdateFoodList extends AsyncTask<String, Void, ArrayList<Food>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<Food> doInBackground(String... params) {
            return DietDataManager.getFoodList(params[0]);
        }

        @Override
        protected void onPostExecute(final ArrayList<Food> foodList) {
            if (foodList != null) {
                progressDialog.dismiss();
                ArrayList<String> foodNameList = new ArrayList<>();
                for (Food food : foodList) {
                    foodNameList.add(food.getName());
                }
                listView.setAdapter(new ArrayAdapter<String>(getActivity(), R.layout.layout_food_list_item,
                        R.id.foodItemTextView, foodNameList));
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Food selectedFood = foodList.get(position);
                        FoodDetailFragment foodDetailFragment = new FoodDetailFragment();
                        Bundle args = new Bundle();
                        args.putString("foodName", selectedFood.getName());
                        args.putString("serveSize", selectedFood.getServeSize());
                        args.putInt("foodId", selectedFood.getFoodId());
                        foodDetailFragment.setArguments(args);
                        getFragmentManager().beginTransaction().replace(R.id.container,
                                foodDetailFragment, null).addToBackStack("foodList").commit();
                    }
                });
            } else {
                progressDialog.dismiss();
                SimpleInfoDialog dialog = new SimpleInfoDialog();
                dialog.setTitle("Error").setMessage("Can not get food data. Please try again later.");
                dialog.show(getFragmentManager(), "error");
            }
        }
    }

}
