package com.caloriecounter.Fragments;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.caloriecounter.Dialogs.SimpleInfoDialog;
import com.caloriecounter.MainActivity;
import com.caloriecounter.R;
import com.caloriecounter.Utils.Register;
import com.caloriecounter.Utils.TimeConvertion;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class PersonalInfoFragment extends Fragment {

    private TextView nicknameView;
    private TextView dateOfBirthView;
    private TextView heightView;
    private TextView weightView;
    private TextView stepsPerMileView;
    private RadioGroup genderView;
    private Button registerFinishButton;
    private Spinner activityLevelView;
    private ArrayAdapter<CharSequence> spinnerAdapter;
    private ProgressDialog progressDialog;

    public PersonalInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_personal_info, container, false);
        nicknameView = (TextView) view.findViewById(R.id.personalInfoFragmentNickname);
        dateOfBirthView = (TextView) view.findViewById(R.id.personalInfoFragmentDOB);
        heightView = (TextView) view.findViewById(R.id.personalInfoFragmentHeight);
        weightView = (TextView) view.findViewById(R.id.personalInfoFragmentWeight);
        stepsPerMileView = (TextView) view.findViewById(R.id.personalInfoFragmentStepsPerMile);
        genderView = (RadioGroup) view.findViewById(R.id.personalInfoFragmentGender);
        registerFinishButton = (Button) view.findViewById(R.id.personalInfoFragmentRegisterButton);
        activityLevelView = (Spinner) view.findViewById(R.id.personalInfoFragmentActivityLevel);
        setUp();
        dateOfBirthView.setInputType(InputType.TYPE_NULL);
        return view;
    }

    private void setUp() {
        // Initiate dropdown list

        spinnerAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.ActivityLevel, android.R.layout.simple_spinner_item);
        final AdapterView.OnItemSelectedListener spinnerListener = new AdapterView.OnItemSelectedListener() {
            private boolean firstTime = true;
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!firstTime) {
                    checkActivityLevel();
                }
                firstTime = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activityLevelView.setAdapter(spinnerAdapter);
        activityLevelView.setOnItemSelectedListener(spinnerListener);
        // Initiate gender
        genderView.check(R.id.personalInfoFragmentGenderMale);
        // Intiate listeners
        dateOfBirthView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    showDatePickerDialog();
                if (!hasFocus)
                    checkDateOfBirth();
            }
        });
        dateOfBirthView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });
        dateOfBirthView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    heightView.requestFocus();
                    return true;
                }
                return false;
            }
        });
        heightView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    checkHeight();
            }
        });
        heightView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    weightView.requestFocus();
                    return true;
                }
                return false;
            }
        });
        weightView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    checkWeight();
            }
        });
        stepsPerMileView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    checkStepsPerMile();
            }
        });
        nicknameView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus)
                    checkNickname();
            }
        });
        registerFinishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkInfo()) {
                    doRegister();
                }
            }
        });
    }

    private void doRegister() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Registering...");
        progressDialog.show();
        Bundle args = getArguments();
        String username = args.getString("username");
        String password = args.getString("password");
        String nickname = nicknameView.getText().toString();
        String gender;
        if (genderView.getCheckedRadioButtonId() == R.id.personalInfoFragmentGenderMale)
            gender = "M";
        else
            gender = "F";
        String dob = dateOfBirthView.getText().toString();
        String height = heightView.getText().toString();
        String weight = weightView.getText().toString();
        String stepsPerMile = stepsPerMileView.getText().toString();
        String activityLevel = Integer.toString(activityLevelView.getSelectedItemPosition());
        RegisterUserTask task = new RegisterUserTask();
        task.execute(username, password, nickname, gender, dob, height, weight, stepsPerMile, activityLevel);
    }

    private class RegisterUserTask extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            return Register.doRegister(params[0], params[1], params[2], params[3], params[4],
                    params[5], params[6], params[7], params[8]);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result != null && result) {
                progressDialog.dismiss();
                Fragment fragment = new LoginFragment();
                fragment.setArguments(getArguments());
                getFragmentManager().beginTransaction().replace(R.id.loginActivityFrame, new LoginFragment()).commit();
            }
            else {
                progressDialog.dismiss();
                SimpleInfoDialog dialog = new SimpleInfoDialog();
                dialog.setTitle("Error!").setMessage("Unknown error occured, please try again later.");
                dialog.show(getFragmentManager(), "error");
            }
        }
    }



    private void showDatePickerDialog() {
        DatePickerFragment datePicker = new DatePickerFragment();
        Bundle args = new Bundle();
        args.putInt("callBackViewId", R.id.personalInfoFragmentDOB);
        datePicker.setArguments(args);
        datePicker.show(getFragmentManager(), null);
    }

    private boolean checkActivityLevel() {
        if (activityLevelView.getSelectedItemPosition() == 0) {
            View selectedView = activityLevelView.getSelectedView();
            if (selectedView instanceof TextView) {
                // Because you can not see the actual error message,
                // So just set error to empty string.
                ((TextView) selectedView).setError("");
            }
            return false;
        }
        return true;
    }

    private boolean checkNickname() {
        if (nicknameView.length() == 0) {
            nicknameView.setError("Nickname can not be empty.");
            return false;
        }
        if (nicknameView.length() < 3) {
            nicknameView.setError("Nickname must longer than 2 characters.");
            return false;
        }
        return true;
    }

    private boolean checkHeight() {
        if (heightView.length() == 0) {
            heightView.setError("Height can not be empty.");
            return false;
        }
        int height = Integer.parseInt(heightView.getText().toString());
        if (height < 80 || height > 230) {
            heightView.setError("Height must between 80 and 230 cm.");
            return false;
        }
        return true;
    }

    private boolean checkWeight() {
        if (weightView.length() == 0) {
            weightView.setError("Weight can not be empty.");
            return false;
        }
        int weight = Integer.parseInt(weightView.getText().toString());
        if (weight < 30 || weight > 500) {
            weightView.setError("Weight must between 30 and 500 kg.");
            return false;
        }
        return true;
    }

    private boolean checkStepsPerMile() {
        if (stepsPerMileView.length() == 0 || stepsPerMileView.getText().length() == 0) {
            Log.d("PersonalInfoFragment","stepspermile length is 0.");
            stepsPerMileView.setError("Steps per mile can not be empty.");
            return false;
        }
        int stepsPerMile = Integer.parseInt(stepsPerMileView.getText().toString());
        if (stepsPerMile < 1000 || stepsPerMile > 5000) {
            stepsPerMileView.setError("Steps per mile must between 1000 and 5000.");
            return false;
        }
        return true;
    }

    private boolean checkDateOfBirth() {
        if (dateOfBirthView.length() == 0) {
            dateOfBirthView.setError("Date of birth can not be empty.");
            return false;
        }
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        int currentYear = calendar.get(Calendar.YEAR);
        calendar.setTime(TimeConvertion.parseNormalDateFormat(dateOfBirthView.getText().toString()));
        if (calendar.get(Calendar.YEAR) >= (currentYear - 6)) {
            dateOfBirthView.setError("Year of birth must be 6 years ago.");
            return false;
        }
        // Because modified the onfocus behaviour, error can not be cleared.
        // So manually clear the error.
        dateOfBirthView.setError(null);
        return true;
    }

    private boolean checkInfo() {
        boolean result = checkDateOfBirth();
        result &= checkNickname();
        result &= checkStepsPerMile();
        result &= checkWeight();
        result &= checkHeight();
        result &= checkActivityLevel();
        return result;
    }

}
