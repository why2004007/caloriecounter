package com.caloriecounter.Fragments;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.caloriecounter.R;

import java.util.ArrayList;


public class AddFoodFragment extends Fragment {

    private ListView listView;

    public AddFoodFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_food, container, false);
        listView = (ListView) view.findViewById(R.id.dietFragmentListView);
        String foodCategories[] = getResources().getStringArray(R.array.FoodCategory);
        ArrayList<String> foodCategoriesArray = new ArrayList<String>();
        for (String category : foodCategories) {
            foodCategoriesArray.add(category);
        }
        ArrayAdapter<String> foodCategorieslist = new ArrayAdapter<String>(getActivity(),
                R.layout.list_item, R.id.foodItemTextView, foodCategoriesArray);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String category = parent.getItemAtPosition(position).toString();
                FoodListFragment foodListFragment = new FoodListFragment();
                Bundle args = new Bundle();
                args.putString("category", category);
                foodListFragment.setArguments(args);
                // Critical part of code!!!!
                // Replace the fragment with the main fragmentManager not the viewPager One
                getActivity().getFragmentManager().beginTransaction().replace(R.id.container,
                        foodListFragment).addToBackStack("foodCategory").commit();

            }
        });
        listView.setAdapter(foodCategorieslist);
        return view;
    }


}
