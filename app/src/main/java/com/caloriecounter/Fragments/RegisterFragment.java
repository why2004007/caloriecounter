package com.caloriecounter.Fragments;


import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.caloriecounter.R;
import com.caloriecounter.Utils.Register;

import org.w3c.dom.Text;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterFragment extends Fragment {

    private TextView usernameView;
    private TextView passwordView;
    private TextView confirmPasswordView;
    private Button registerButton;
    private boolean readyToSubmit = false;

    public RegisterFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_register, container, false);
        registerButton = (Button) view.findViewById(R.id.registerButton);
        usernameView = (TextView) view.findViewById(R.id.registerFragmentUsername);
        passwordView = (TextView) view.findViewById(R.id.registerFragmentPassword);
        confirmPasswordView = (TextView) view.findViewById(R.id.registerFragmentConfirmPassword);
        setUpListeners();
        return view;
    }

    private void setUpListeners() {
        usernameView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (! hasFocus) {
                    checkUsername();
                }
            }
        });
        passwordView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (! hasFocus) {
                    checkPassword();
                }
            }
        });
        confirmPasswordView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (! hasFocus)
                    checkConfirmPassword();
            }
        });
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doRegister();
            }
        });
    }

    private void doRegister() {

        if (checkInfo() && readyToSubmit) {
            PersonalInfoFragment personalInfoFragment = new PersonalInfoFragment();
            Bundle bundle = new Bundle();
            bundle.putString("username", usernameView.getText().toString());
            bundle.putString("password", passwordView.getText().toString());
            personalInfoFragment.setArguments(bundle);
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.loginActivityFrame, personalInfoFragment).addToBackStack("registerFragment").commit();
        }
    }

    private void verifyUsername() {
        CheckUserValidTask task = new CheckUserValidTask();
        task.execute(new String[] {usernameView.getText().toString()});
    }

    private class CheckUserValidTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... params) {
            String username = params[0];
            return Register.checkUsernameValid(username);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            Log.i("onOnlineCheckUsername","Check username succeed.");
            if (result != null && ! result) {
                usernameView.setError("Username has been used.");
                readyToSubmit = false;
            }
            else
                readyToSubmit = true;
        }
    }

    private boolean checkPassword() {
        boolean result = true;
        if (passwordView.length() < 6) {
            passwordView.setError("Password must longer than 5.");
            result = false;
        }
        return result;
    }

    private boolean checkConfirmPassword() {
        boolean result = true;
        if (!passwordView.getText().toString().equals(confirmPasswordView.getText().toString())) {
            confirmPasswordView.setError("Password mismatch");
            result = false;
        }
        return result;
    }

    private boolean checkUsername() {
        boolean result = true;
        if (usernameView.length() == 0) {
            usernameView.setError("Username is empty.");
            result = false;
        }
        if (usernameView.length() < 5) {
            usernameView.setError("Username must longer than 4.");
            result = false;
        }
        if (usernameView.getText().toString().matches("\\d*")) {
            usernameView.setError("Username shouldn't be pure digits.");
            result = false;
        }
        if (result) {
            verifyUsername();
        }
        return result;
    }

    private boolean checkInfo() {
        boolean result = checkUsername();
        result = checkPassword();
        result = checkConfirmPassword();
        return result;
    }
}
