package com.caloriecounter.Fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.TextView;

import com.caloriecounter.R;

import java.util.Calendar;

/**
 * Created by Daniel on 14/09/2015.
 */
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private TextView callBackView;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        Bundle args = getArguments();
        int resourceId = args.getInt("callBackViewId");
        callBackView = (TextView) getActivity().findViewById(resourceId);
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        String timeString = Integer.toString(year) + "-" + String.format("%02d", month + 1)
                + "-" + String.format("%02d", day);
        callBackView.setText(timeString);
    }

    @Override
    public void onDestroyView() {
        Log.d("DatePickerFragment", "DatePickerFragment on destroy called.");
        super.onDestroyView();
    }
}