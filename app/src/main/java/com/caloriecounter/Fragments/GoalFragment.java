package com.caloriecounter.Fragments;


import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.caloriecounter.Dialogs.InputGoalDialog;
import com.caloriecounter.Dialogs.OnQuitListener;
import com.caloriecounter.Dialogs.SimpleInfoDialog;
import com.caloriecounter.Objects.User;
import com.caloriecounter.R;
import com.caloriecounter.Utils.CalorieDataManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class GoalFragment extends Fragment {

    private TextView dailyBurnedView;
    private TextView consumedView;
    private TextView remainingView;
    private TextView goalView;
    private RelativeLayout infoFrame;
    private ProgressDialog progressDialog;

    public GoalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_goal, container, false);
        dailyBurnedView = (TextView) view.findViewById(R.id.goalFragmentDailyBurned);
        consumedView = (TextView) view.findViewById(R.id.goalFragmentConsumed);
        remainingView = (TextView) view.findViewById(R.id.goalFragmentRemaining);
        goalView = (TextView) view.findViewById(R.id.goalFragmentGoal);
        infoFrame = (RelativeLayout) view.findViewById(R.id.goalFragmentInfoFrame);
        infoFrame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSetGoalDialog();
            }
        });
        UpdateInterface task = new UpdateInterface();
        task.execute();
        return view;
    }

    private void setGoalAnimation(int goal) {
        ValueAnimator animator = ValueAnimator.ofInt(0, goal);
        animator.setDuration(3000);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                goalView.setText(String.format("%04d", value));
            }
        });
        animator.setInterpolator(new DecelerateInterpolator((float)1.8));
        animator.start();
    }

    private void showSetGoalDialog() {
        InputGoalDialog dialog = new InputGoalDialog();
        dialog.setDialogOnQuitListener(new DialogOnQuitListener());
        dialog.show(getActivity().getFragmentManager(), "inputGoal");
    }

    private class UpdateInterface extends AsyncTask<Void, Void, Bundle> {

        @Override
        protected void onPreExecute() {
            double dailyBurned = User.getInstance().getDailyBurned()/1000.0;
            dailyBurnedView.setText(String.format("%.2fk", dailyBurned));
        }

        @Override
        protected Bundle doInBackground(Void... params) {
            if (User.getInstance().getGoal() == 0)
                CalorieDataManager.getGoal();
            // Check if goal has been retrieved.
            SimpleInfoDialog dialog = new SimpleInfoDialog();
            dialog.setTitle("Goal hasn't set").setMessage("You haven't set your goal, please set your goal by click the circle.")
                    .setPositiveButtonText("Yes").setHasNegativeButton(false);
            if (User.getInstance().getGoal() == 0)
                dialog.show(getFragmentManager(),null);
            double remaining = CalorieDataManager.getRemaining(getActivity());
            double consumed = CalorieDataManager.getTotalConsumed();
            Bundle args = new Bundle();
            args.putDouble("remaining",remaining);
            args.putDouble("consumed",consumed);
            return args;
        }

        @Override
        protected void onPostExecute(Bundle result) {
            setGoalAnimation((int) User.getInstance().getGoal());
            //goalView.setText(String.format("%04d",(int) User.getInstance().getGoal()));
            double remaining = result.getDouble("remaining");
            double steps = remaining / User.getInstance().getCaloriePerStep();
            if (steps > 99000 ) {
                remainingView.setText(">99k");
            } else if (steps > 30000 ) {
                remainingView.setText(String.format("%dk", (int) steps / 1000));
            } else if (steps > 1000) {
                remainingView.setText(String.format("%.2fk", steps / 1000.0));
            } else if (steps > 0){
                remainingView.setText(Integer.toString((int) steps));
            } else {
                remainingView.setText(Integer.toString(0));
            }
            double consumed = result.getDouble("consumed");
            if (consumed > 1000)
                consumedView.setText(String.format("%.2fk", (int) consumed/1000.0));
            else
                consumedView.setText(String.format("%d", (int) consumed));
        }
    }


    private class UpdateGoalTask extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Updating goal info....");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            CalorieDataManager.updateReports(getActivity(), true);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            progressDialog.dismiss();
            UpdateInterface updateInterface = new UpdateInterface();
            updateInterface.execute();
        }
    }


    public class DialogOnQuitListener extends OnQuitListener {

        public void onQuit() {
            Log.d("onQuitListener", "Dialog quited.");
            UpdateGoalTask task = new UpdateGoalTask();
            task.execute();
        }
    }
}
