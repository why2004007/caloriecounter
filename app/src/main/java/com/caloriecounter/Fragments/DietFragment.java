package com.caloriecounter.Fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.caloriecounter.R;

/**
 * The viewpager are first created by google's official code.
 * Then fully modified by referencing from stackoverflow.
 * The code is fully rewritten.
 */
public class DietFragment extends Fragment implements ViewPager.OnPageChangeListener {

    private ViewPager viewPager;
    private ViewPagerAdapter adapter;

    public DietFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_diet, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.dietFragmentViewPager);
        // Use child fragment manager instead of the Activity's, or the back stack will be empty
        adapter = new ViewPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(adapter);

        return view;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        String title = null;
        if (position == 0) {
            title = "Consumed";
        } else {
            title = "Add Consumed";
        }
        getActivity().getActionBar().setTitle(title);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            if (i == 0) {
                return new ConsumedFragment();
            } else {
                return new AddFoodFragment();
            }
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
