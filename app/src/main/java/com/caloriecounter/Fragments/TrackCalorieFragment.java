package com.caloriecounter.Fragments;


import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.caloriecounter.Objects.User;
import com.caloriecounter.R;
import com.caloriecounter.Utils.CalorieDataManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class TrackCalorieFragment extends Fragment {

    private ProgressBar progressBar;
    private TextView dateView;
    private TextView totalBurnedView;
    private TextView goalView;
    private TextView basicView;
    private TextView consumedView;
    private TextView walkingView;


    public TrackCalorieFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_track_calorie, container, false);
        progressBar = (ProgressBar) view.findViewById(R.id.trackCalorieFragmentProgress);
        dateView = (TextView) view.findViewById(R.id.trackCalorieFragmentDateView);
        totalBurnedView = (TextView) view.findViewById(R.id.trackCalorieFragmentTotalBurned);
        consumedView = (TextView) view.findViewById(R.id.trackCalorieFragmentConsumedView);
        walkingView = (TextView) view.findViewById(R.id.trackCalorieFragmentWalking);
        goalView = (TextView) view.findViewById(R.id.trackCalorieFragmentGoalView);
        basicView = (TextView) view.findViewById(R.id.trackCalorieFragmentBasicView);
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd", Locale.ENGLISH);
        dateView.setText(dateFormat.format(new Date()));
        setUpComponent();
        UpdateInterfaceTask task = new UpdateInterfaceTask();
        task.execute();
        return view;
    }

    public void setProgressBarValue(int progress) {
        ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", 0, progress);
        animation.setDuration(3000);
        animation.setInterpolator(new DecelerateInterpolator(2));
        animation.start();
    }

    public void setTotalBurnedValue(final int value) {
        ValueAnimator animator = ValueAnimator.ofInt(0, value);
        animator.setDuration(3000);
        animator.setInterpolator(new DecelerateInterpolator((float) 1.5));
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                totalBurnedView.setText(String.format("%04d", (Integer) animation.getAnimatedValue()));
            }
        });
        animator.start();
    }

    private void setUpComponent() {
        if (User.isFullyInitialised()) {
            double totalBurned = CalorieDataManager.getTotalBurned(getActivity());
            if (User.getInstance().getGoal() != 0)
                setProgressBarValue((int) (totalBurned / User.getInstance().getGoal() * 10000));
            else{
                Toast.makeText(getActivity(), "You haven't set your goal!", Toast.LENGTH_SHORT).show();
                setProgressBarValue(0);
            }
            setTotalBurnedValue((int) CalorieDataManager.getTotalBurned(getActivity()));
            //totalBurnedView.setText(String.format("%04d", (int) CalorieDataManager.getTotalBurned(getActivity())));
            goalView.setText(String.format("Goal: %1$d kCal", (int) User.getInstance().getGoal()));
            double dailyBurned = User.getInstance().getDailyBurned()/1000.0;
            basicView.setText(String.format("%.2fk", dailyBurned));
        }
        else {
            Toast.makeText(getActivity(), "Can not get remote user info.", Toast.LENGTH_SHORT).show();
        }
    }

    private class UpdateInterfaceTask extends AsyncTask<Void, Void, Bundle> {

        @Override
        protected Bundle doInBackground(Void... params) {
            Bundle arguments = new Bundle();
            arguments.putDouble("consumed", CalorieDataManager.getTotalConsumed());
            Log.d("test", Double.toString(CalorieDataManager.getBurnedByStep(getActivity())));
            arguments.putDouble("walking", CalorieDataManager.getBurnedByStep(getActivity()));
            return arguments;
        }

        @Override
        protected void onPostExecute(Bundle result) {
            double consumed = result.getDouble("consumed");
            if (consumed > 1000)
                consumedView.setText(String.format("%.2fk", consumed/1000.0));
            else
                consumedView.setText(String.format("%d", (int) consumed));
            double walking = result.getDouble("walking");
            if (walking > 1000)
                walkingView.setText(String.format("%.2fk", walking/1000.0));
            else
                walkingView.setText(String.format("%d", (int) walking));
        }
    }

}
