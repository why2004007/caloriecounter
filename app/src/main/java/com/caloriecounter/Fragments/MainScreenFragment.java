package com.caloriecounter.Fragments;


import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.caloriecounter.Dialogs.InputCustomWordsDialog;
import com.caloriecounter.Dialogs.InputGoalDialog;
import com.caloriecounter.Objects.User;
import com.caloriecounter.R;
import com.caloriecounter.Utils.CalorieDataManager;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainScreenFragment extends Fragment {

    private ProgressBar progressBar;
    private TextView dateView;
    private TextView totalBurnedView;
    private TextView goalView;
    private TextView welcomeMessageView;
    private Button shareToFacebookButton;
    private TextView customWordsView;

    public MainScreenFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_screen, container, false);
        progressBar = (ProgressBar) view.findViewById(R.id.mainScreenFragmentProgressBar);
        dateView = (TextView) view.findViewById(R.id.mainScreenFragmentDate);
        totalBurnedView = (TextView) view.findViewById(R.id.mainScreenFragmentCurrentBurned);
        goalView = (TextView) view.findViewById(R.id.mainScreenFragmentGoal);
        welcomeMessageView = (TextView) view.findViewById(R.id.mainScreenFragmentWelcome);
        welcomeMessageView.setText("Hi " + User.getInstance().getNickname() + "~");
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd", Locale.ENGLISH);
        dateView.setText(dateFormat.format(new Date()));
        shareToFacebookButton = (Button) view.findViewById(R.id.mainScreenFragmentShareFacebook);
        shareToFacebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = getActivity().getWindow().getDecorView().getRootView();
                view.setDrawingCacheEnabled(true);
                Bitmap image = Bitmap.createBitmap(view.getDrawingCache());
                view.setDrawingCacheEnabled(false);
                SharePhoto photo = new SharePhoto.Builder().setBitmap(image).build();
                SharePhotoContent content = new SharePhotoContent.Builder().addPhoto(photo).build();
                ShareDialog.show(getActivity(), content);
            }
        });
        customWordsView = (TextView) view.findViewById(R.id.mainScreenFragmentCustomWords);
        customWordsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputCustomWordsDialog dialog = new InputCustomWordsDialog();
                dialog.setDialogOnQuitListener(new OnCustomWordSetListener());
                dialog.show(getFragmentManager(), null);
            }
        });
        updateCustomWords();
        FullyInitialiseUserTask task = new FullyInitialiseUserTask();
        task.execute();
        //UpdateInterfaceTask updateInterfaceTask = new UpdateInterfaceTask();
        //updateInterfaceTask.execute();
        return view;
    }

    public void updateCustomWords(){
        SharedPreferences userSpecifiedPreference = getActivity().getSharedPreferences(
                Integer.toString(User.getInstance().getUserId()), Context.MODE_PRIVATE);
        String customWords = userSpecifiedPreference.getString("customWords",
                getString(R.string.default_main_fragement_words));
        customWordsView.setText(customWords);
    }

    public void setTotalBurnedValue(final int value) {
        ValueAnimator animator = ValueAnimator.ofInt(0, value);
        animator.setDuration(3000);
        animator.setInterpolator(new DecelerateInterpolator((float) 1.5));
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                totalBurnedView.setText(String.format("%04d", (Integer) animation.getAnimatedValue()));
            }
        });
        animator.start();
    }

    public void setProgressBarValue(int progress) {
        ObjectAnimator animation = ObjectAnimator.ofInt(progressBar, "progress", 0, progress);
        animation.setDuration(3000);
        animation.setInterpolator(new DecelerateInterpolator(2));
        animation.start();
    }

    private class UpdateInterfaceTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            if (User.isFullyInitialised()) {
                double totalBurned = CalorieDataManager.getTotalBurned(getActivity());
                if (User.getInstance().getGoal() != 0)
                    setProgressBarValue((int) (totalBurned / User.getInstance().getGoal() * 10000));
                else{
                    Toast.makeText(getActivity(), "You haven't set your goal!", Toast.LENGTH_SHORT).show();
                    setProgressBarValue(0);
                }
                setTotalBurnedValue((int)CalorieDataManager.getTotalBurned(getActivity()));
                //totalBurnedView.setText(String.format("%04d", (int) CalorieDataManager.getTotalBurned(getActivity())));
                goalView.setText(String.format("Goal: %1$d kCal", (int) User.getInstance().getGoal()));
            }
            else {
                Toast.makeText(getActivity(), "Can not get remote user info.", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
        }
    }


    private class FullyInitialiseUserTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {
            if(User.isFullyInitialised())
                return true;

            return User.isFullyInitialised();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                UpdateInterfaceTask updateInterfaceTask = new UpdateInterfaceTask();
                updateInterfaceTask.execute();
            }
            else {
                Toast.makeText(getActivity(), "User not initialised.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class OnCustomWordSetListener {
        public void onSet() {
            updateCustomWords();
        }
    }

}
