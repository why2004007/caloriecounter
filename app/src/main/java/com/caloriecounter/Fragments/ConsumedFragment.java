package com.caloriecounter.Fragments;


import android.animation.ValueAnimator;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

import com.caloriecounter.Objects.User;
import com.caloriecounter.R;
import com.caloriecounter.Utils.CalorieDataManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConsumedFragment extends Fragment {

    private TextView totalConsumedView;
    private TextView remainingView;
    private TextView goalView;
    private TextView burnedView;

    public ConsumedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_consumed, container, false);
        totalConsumedView = (TextView) view.findViewById(R.id.consumedFragmentTotalConsumed);
        remainingView = (TextView) view.findViewById(R.id.consumedFragmentRemaining);
        goalView = (TextView) view.findViewById(R.id.consumedFragmentGoal);
        burnedView = (TextView) view.findViewById(R.id.consumedFragmentTotalBurned);
        UpdateComponentsTask task = new UpdateComponentsTask();
        task.execute();
        return view;
    }


    private void setConsumedValue(Integer value) {
        ValueAnimator animator = ValueAnimator.ofInt(0, value);
        animator.setDuration(3000);
        animator.setInterpolator(new DecelerateInterpolator((float) 1.5));
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                totalConsumedView.setText(String.format("%04d", (Integer) animation.getAnimatedValue()));
            }
        });
        animator.start();
    }

    private class UpdateComponentsTask extends AsyncTask<Void, Void, Bundle> {

        @Override
        protected Bundle doInBackground(Void... params) {
            Bundle args = new Bundle();
            Double totalBurned = CalorieDataManager.getTotalBurned(getActivity());
            args.putDouble("totalBurned", totalBurned);
            Double remaining = CalorieDataManager.getRemaining(getActivity());
            args.putDouble("remaining", remaining);
            Double totalConsumed = CalorieDataManager.getTotalConsumed();
            args.putDouble("totalConsumed", totalConsumed);
            if (!User.isFullyInitialised()) {
                CalorieDataManager.fullyInitialiseUserData(getActivity());
            }
            return args;
        }

        @Override
        protected void onPostExecute(Bundle bundle) {
            if (User.getInstance().getGoal() > 1000)
                goalView.setText(String.format("%.2fk", User.getInstance().getGoal()/1000.0));
            else
                goalView.setText(String.format("%d", (int) User.getInstance().getGoal()));
            double remaining = bundle.getDouble("remaining");
            if (remaining > 1000)
                remainingView.setText(String.format("%.2fk", remaining/1000.0));
            else if (remaining < 0)
                remainingView.setText("0");
            else
                remainingView.setText(String.format("%d", (int) remaining));
            double totalBurned = bundle.getDouble("totalBurned");
            if (totalBurned > 1000)
                burnedView.setText(String.format("%.2fk", totalBurned / 1000.0));
            else
                burnedView.setText(String.format("%d", (int) totalBurned));
            double totalConsumed = bundle.getDouble("totalConsumed");
            //totalConsumedView.setText(String.format("%04d", (int) totalConsumed));
            setConsumedValue((int) totalConsumed);
        }
    }



}
