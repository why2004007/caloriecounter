package com.caloriecounter.Fragments;


import android.animation.ValueAnimator;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.caloriecounter.Dialogs.InputStepsDialog;
import com.caloriecounter.Dialogs.OnQuitListener;
import com.caloriecounter.Objects.User;
import com.caloriecounter.R;
import com.caloriecounter.Utils.CalorieDataManager;
import com.caloriecounter.Utils.Database;
import com.caloriecounter.Utils.Register;
import com.caloriecounter.Utils.TimeConvertion;

import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class StepsFragment extends Fragment {

    private RelativeLayout progressBarArea;
    private TextView stepsTextView;
    private TextView distanceTextView;
    private TextView calPerMileTextView;
    private TextView stepsRemainingTextView;

    public StepsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_steps, container, false);
        progressBarArea = (RelativeLayout) view.findViewById(R.id.stepsFragmentProgressBarArea);
        stepsTextView = (TextView) view.findViewById(R.id.stepsFragmentStepsTextView);
        distanceTextView = (TextView) view.findViewById(R.id.stepsFragmentDistanceTextView);
        calPerMileTextView = (TextView) view.findViewById(R.id.stepsFragmentCaloriePerMileTextView);
        stepsRemainingTextView = (TextView) view.findViewById(R.id.stepsFragmentStepsRemainingTextView);
        UpdateInformationTask task = new UpdateInformationTask();
        task.execute();
        progressBarArea.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputStepsDialog dialog = new InputStepsDialog();
                dialog.setOnQuitListener(new DialogOnQuitListener());
                dialog.show(getFragmentManager(), null);
            }
        });
        return view;
    }

    public void setStepsValue(final int value) {
        ValueAnimator animator = ValueAnimator.ofInt(0, value);
        animator.setDuration(3000);
        animator.setInterpolator(new DecelerateInterpolator((float) 1.5));
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                stepsTextView.setText(String.format("%04d", (Integer) animation.getAnimatedValue()));
            }
        });
        animator.start();
    }

    private class UpdateInformationTask extends AsyncTask<Void, Void, Integer> {

        @Override
        protected void onPreExecute() {
            calPerMileTextView.setText(String.format("%d", (int) (User.getInstance().getCaloriePerStep()
                    * User.getInstance().getStepsPerMile())));
        }

        @Override
        protected Integer doInBackground(Void... params) {
            return Database.getTotalSteps(getActivity(), User.getInstance(), new Date(), new Date());
        }

        @Override
        protected void onPostExecute(Integer integer) {
            setStepsValue(integer);
            //stepsTextView.setText(String.format("%04d", integer));
            double remaining = User.getInstance().getGoal() + User.getInstance().getTotalConsumed()
                    - User.getInstance().getDailyBurned() - CalorieDataManager.getBurnedByStep(getActivity());
            double steps = remaining / User.getInstance().getCaloriePerStep();
            if (steps > 1000) {
                stepsRemainingTextView.setText(String.format("%.2fk", steps / 1000.0));
            } else if (steps < 0) {
                stepsRemainingTextView.setText("0");
            } else {
                stepsRemainingTextView.setText(Integer.toString((int) steps));
            }
            double distance = (double) integer / User.getInstance().getStepsPerMile();
            if (distance > 100) {
                distanceTextView.setText(String.format("%d", (int) distance));
            }else if (distance > 10) {
                distanceTextView.setText(String.format("%.1f", distance));
            } else if (distance == 0) {
                distanceTextView.setText("0");
            } else {
                distanceTextView.setText(String.format("%.2f", distance));
            }
        }
    }

    private class DialogOnQuitListener extends OnQuitListener {

        @Override
        public void onQuit() {
            UpdateInformationTask task = new UpdateInformationTask();
            task.execute();
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }
}
