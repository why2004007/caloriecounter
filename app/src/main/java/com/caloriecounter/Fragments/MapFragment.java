package com.caloriecounter.Fragments;


import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.caloriecounter.Objects.User;
import com.caloriecounter.R;
import com.caloriecounter.Utils.GoogleServices;
import com.mapquest.android.maps.AnnotationView;
import com.mapquest.android.maps.DefaultItemizedOverlay;
import com.mapquest.android.maps.GeoPoint;
import com.mapquest.android.maps.MapView;
import com.mapquest.android.maps.Overlay;
import com.mapquest.android.maps.OverlayItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment {

    private MapView mapView;
    private GeoPoint defaultGeoPoint = new GeoPoint(-37.876470,145.044078);
    private AnnotationView annotation;
    private final int NUMBER_OF_OBJECTS_PER_SCREEN = 60;
    private GetParksTask getParksTask;
    private ProgressDialog progressDialog;
    public MapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.getController().setZoom(13);
        mapView.setBuiltInZoomControls(true);
        if (User.getInstance().getLongitude() != 0 && User.getInstance().getLongitude() != 0) {
            mapView.getController().setCenter(new GeoPoint(User.getInstance().getLatitude(),
                    User.getInstance().getLongitude()));
            drawPOIMarker(true, User.getInstance().getLatitude(), User.getInstance().getLongitude(), "Myself", null);
        } else {
            mapView.getController().setCenter(defaultGeoPoint);
            drawPOIMarker(true, defaultGeoPoint.getLatitude(), defaultGeoPoint.getLongitude(), "Myself", null);
        }
        annotation = new AnnotationView(mapView);
        getParksTask = new GetParksTask();
        getParksTask.execute(mapView.getMapCenter().getLatitude(), mapView.getMapCenter().getLongitude());
        return view;
    }

    private void drawPOIMarker(boolean self, double latitude, double longitude, String title, String description) {
        if (! this.isDetached()) {
            Drawable marker = null;
            // These two map markers, which resource can be found from drawable are downloaded from
            // http://www.flaticon.com/, these icons are free to use without copyrights
            if (self) {
                marker = getResources().getDrawable(R.drawable.self, null);
            } else {
                marker = getResources().getDrawable(R.drawable.park, null);
            }
            final DefaultItemizedOverlay poiOverlay = new DefaultItemizedOverlay(marker);
            OverlayItem item = new OverlayItem(new GeoPoint(latitude, longitude), title, description);
            poiOverlay.addItem(item);
            poiOverlay.setTapListener(new Overlay.OverlayTapListener() {
                private boolean isShow = true;

                @Override
                public void onTap(GeoPoint geoPoint, MapView mapView) {
                    int touchedIndex = poiOverlay.getLastFocusedIndex();
                    if (touchedIndex > -1) {
                        OverlayItem touched = poiOverlay.getItem(touchedIndex);
                        if (isShow) {
                            annotation.showAnnotationView(touched);
                            isShow = false;
                        } else {
                            annotation.hide();
                            isShow = true;
                        }
                    }
                }
            });
            mapView.getOverlays().add(poiOverlay);
        }
    }

    private void parsePOIList(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i ++) {
            if (i > NUMBER_OF_OBJECTS_PER_SCREEN) {
                break;
            }
            try {
                JSONObject park = jsonArray.getJSONObject(i);
                JSONObject geoInfo = park.getJSONObject("geometry").getJSONObject("location");
                drawPOIMarker(false, geoInfo.getDouble("lat"), geoInfo.getDouble("lng"),
                        park.getString("name"), null);
            } catch (JSONException e) {
                Log.e("onGetParkInfo", e.getMessage(), e);
            }
        }
    }

    private class GetParksTask extends AsyncTask<Double, Void, JSONArray> {

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading Parks Locations ....");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected JSONArray doInBackground(Double... params) {
            return GoogleServices.searchPlaces("park", 5000, params[0],params[1]);
        }

        @Override
        protected void onPostExecute(JSONArray jsonArray) {
            progressDialog.dismiss();
            parsePOIList(jsonArray);
        }
    }

}
