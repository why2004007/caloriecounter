package com.caloriecounter.Fragments;


import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.caloriecounter.Dialogs.SimpleInfoDialog;
import com.caloriecounter.R;
import com.caloriecounter.Utils.CalorieDataManager;
import com.caloriecounter.Utils.DietDataManager;
import com.caloriecounter.Utils.GoogleServices;
import com.caloriecounter.Utils.HttpConnection;
import com.caloriecounter.Utils.NationalNutrientDatabase;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * A simple {@link Fragment} subclass.
 */
public class FoodDetailFragment extends Fragment {

    private ProgressBar foodImageLoading;
    private ProgressBar descriptionLoading;
    private ProgressBar nutrientsLoading;
    private ImageView imageView;
    private TextView descriptionView;
    private TextView foodNameView;
    private EditText quantityView;
    private EditText serveSizeView;
    private Button addToConsumedButton;
    private TextView energyView;
    private TextView proteinView;
    private TextView carbohydrateView;
    private TextView fatView;
    private TextView sodiumView;
    private int foodId;
    private String foodName;
    private LinearLayout nutrientsContainer;
    private ProgressDialog progressDialog;
    private int retryTimes = 0;

    public FoodDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_food_detail, container, false);
        setUpComponents(view);
        Bundle args = getArguments();
        foodName = args.getString("foodName");
        foodId = args.getInt("foodId");
        foodNameView.setText(foodName);
        serveSizeView.setText(args.getString("serveSize"));
        UpdateComponentsTask task = new UpdateComponentsTask();
        task.execute(foodName);
        RetrieveNutrientsTask nutrientsTask = new RetrieveNutrientsTask();
        nutrientsTask.execute(false);
        return view;
    }

    private void setUpComponents(View view) {
        foodImageLoading = (ProgressBar) view.findViewById(R.id.foodDetailImageLoading);
        descriptionLoading = (ProgressBar) view.findViewById(R.id.foodDetailDescriptionLoading);
        nutrientsLoading = (ProgressBar) view.findViewById(R.id.foodDetailNutrientsLoading);
        imageView = (ImageView) view.findViewById(R.id.foodDetailImageView);
        descriptionView = (TextView) view.findViewById(R.id.foodDetailFoodDescription);
        quantityView = (EditText) view.findViewById(R.id.foodDetailQuantity);
        serveSizeView = (EditText) view.findViewById(R.id.foodDetailServeSize);
        foodNameView = (TextView) view.findViewById(R.id.foodDetailFoodName);
        addToConsumedButton = (Button) view.findViewById(R.id.foodDetailAddToConsumedButton);
        addToConsumedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double quantity = Double.parseDouble(quantityView.getText().toString());
                if (quantity > 0) {
                    AddToConsumedTask task = new AddToConsumedTask();
                    task.execute(quantity);
                } else {
                    SimpleInfoDialog dialog = new SimpleInfoDialog();
                    dialog.setTitle("Incorrect Information").setMessage("Quantity must greater than 0.");
                    dialog.show(getFragmentManager(), "error");
                }

            }
        });
        energyView = (TextView) view.findViewById(R.id.foodDetailEnergy);
        fatView = (TextView) view.findViewById(R.id.foodDetailFat);
        proteinView = (TextView) view.findViewById(R.id.foodDetailProtein);
        carbohydrateView = (TextView) view.findViewById(R.id.foodDetailCarbohydrate);
        sodiumView = (TextView) view.findViewById(R.id.foodDetailSodium);
        nutrientsContainer = (LinearLayout) view.findViewById(R.id.foodDetailNutrientsList);
        nutrientsContainer.setVisibility(View.INVISIBLE);
    }


    private class AddToConsumedTask extends AsyncTask<Double, Void, Void> {

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Adding consumed...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Double... params) {
            DietDataManager.addConsumedRecord(foodId, params[0]);
            CalorieDataManager.updateReports(getActivity(), false);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();
            getFragmentManager().popBackStack("foodCategory", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    private class UpdateComponentsTask extends AsyncTask<String, Void, Bundle> {

        @Override
        protected Bundle doInBackground(String... params) {
            try {
                return GoogleServices.searchWeb(URLEncoder.encode(params[0].split("\\(")[0], "utf-8"));
            } catch (UnsupportedEncodingException e) {
                Log.e("onEncodeUrl",e.getMessage(),e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bundle bundle) {
            if (bundle != null) {
                RetrieveImageTask task = new RetrieveImageTask();
                task.execute(bundle.getString("imageLink"));
                descriptionView.setText(bundle.getString("description"));
                descriptionLoading.setVisibility(View.INVISIBLE);
            } else {
                SimpleInfoDialog dialog = new SimpleInfoDialog();
                dialog.setTitle("Error").setMessage("Can not retrieve information from Internet.");
                dialog.show(getFragmentManager(), "error");
            }
        }
    }

    private class RetrieveImageTask extends AsyncTask<String, Void, Bitmap> {

        @Override
        protected Bitmap doInBackground(String... params) {
            HttpConnection connection = new HttpConnection(params[0]);
            InputStream stream = connection.streamGET();
            Bitmap bmp = BitmapFactory.decodeStream(stream);
            try {
                stream.close();
            } catch (IOException e) {
                Log.e("onCloseImageStream", e.getMessage(),e );
            }
            return bmp;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            imageView.setImageBitmap(bitmap);
            foodImageLoading.setVisibility(View.INVISIBLE);
        }
    }

    private class RetrieveNutrientsTask extends AsyncTask<Boolean, Void, Bundle> {

        @Override
        protected Bundle doInBackground(Boolean... params) {
            return NationalNutrientDatabase.getNutrients(foodName.split("\\(")[0].replace(" ","+"), params[0]);

        }

        @Override
        protected void onPostExecute(Bundle bundle) {
            if (bundle != null) {
                nutrientsLoading.setVisibility(View.INVISIBLE);
                energyView.setText(bundle.getString("energy"));
                fatView.setText(bundle.getString("fat"));
                proteinView.setText(bundle.getString("protein"));
                carbohydrateView.setText(bundle.getString("carbohydrate"));
                sodiumView.setText(bundle.getString("sodium"));
                nutrientsContainer.setVisibility(View.VISIBLE);
            } else {
                if (retryTimes > 3) {
                    SimpleInfoDialog dialog = new SimpleInfoDialog();
                    dialog.setTitle("Error").setMessage("Can not retrieve nutrients data.");
                    dialog.show(getFragmentManager(), "error");
                } else {
                    retryTimes += 1;
                    RetrieveNutrientsTask task = new RetrieveNutrientsTask();
                    task.execute(true);
                }
            }

        }
    }
}
