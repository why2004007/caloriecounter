package com.caloriecounter.Fragments;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.caloriecounter.Dialogs.SimpleInfoDialog;
import com.caloriecounter.R;
import com.caloriecounter.Utils.CalorieDataManager;
import com.caloriecounter.Utils.TimeConvertion;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.util.Calendar;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportFragment extends Fragment {

    private GraphView graphView;
    private EditText startDateView;
    private EditText endDateView;
    private Button showReportButton;
    private ProgressDialog progressDialog;
    private Date defaultStartDate;
    private Date defaultEndDate;

    public ReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_report, container, false);
        graphView = (GraphView) view.findViewById(R.id.reportFragmentGraphView);
        startDateView = (EditText) view.findViewById(R.id.reportFragmentStartDate);
        endDateView = (EditText) view.findViewById(R.id.reportFragmentEndDate);
        showReportButton = (Button) view.findViewById(R.id.reportFragmentShowReportButton);
        setUpComponents();
        setDefaultDate();
        UpdateInformationTask task = new UpdateInformationTask();
        task.execute(TimeConvertion.getNormalDateFormatString(defaultStartDate),
                TimeConvertion.getNormalDateFormatString(defaultEndDate));
        return view;
    }

    private void setUpComponents() {
        startDateView.setInputType(InputType.TYPE_NULL);
        endDateView.setInputType(InputType.TYPE_NULL);
        showReportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkForm()) {
                    UpdateInformationTask task = new UpdateInformationTask();
                    task.execute(startDateView.getText().toString(), endDateView.getText().toString());
                }
            }
        });
        startDateView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    DatePickerFragment fragment = new DatePickerFragment();
                    Bundle args = new Bundle();
                    args.putInt("callBackViewId", R.id.reportFragmentStartDate);
                    fragment.setArguments(args);
                    fragment.show(getFragmentManager(), null);
                }
            }
        });;
        endDateView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                DatePickerFragment fragment = new DatePickerFragment();
                Bundle args = new Bundle();
                args.putInt("callBackViewId", R.id.reportFragmentEndDate);
                fragment.setArguments(args);
                fragment.show(getFragmentManager(), null);
            }
        });
    }

    private boolean checkForm() {
        String startDateString = startDateView.getText().toString();
        String endDateString = endDateView.getText().toString();
        if (startDateString.length() == 0 && endDateString.length() == 0) {
            if (startDateString.length() == 0) {
                startDateView.setError("You must set an end date");
            }
            if (endDateString.length() == 0) {
                endDateView.setError("You must set an end date.");
            }
            return false;
        }
        Date startDate = TimeConvertion.parseNormalDateFormat(startDateString);
        Date endDate = TimeConvertion.parseNormalDateFormat(endDateString);

        double miliSecondsPerDay = 24*60*60*1000;

        Date currentDate = new Date();
        if (startDate.getTime() > currentDate.getTime()) {
            SimpleInfoDialog dialog = new SimpleInfoDialog();
            dialog.setTitle("Incorrect Information").setMessage("The start date must earlier than today.");
            dialog.show(getFragmentManager(), "error");
            return false;
        }

        double days = (endDate.getTime() - startDate.getTime()) / miliSecondsPerDay;
        if (days < 0) {
            SimpleInfoDialog dialog = new SimpleInfoDialog();
            dialog.setTitle("Incorrect Information").setMessage("End date must earlier than start date.");
            dialog.show(getFragmentManager(), "error");
            return false;
        } else if (days > 31) {
            SimpleInfoDialog dialog = new SimpleInfoDialog();
            dialog.setTitle("Incorrect Information").setMessage("The date range must less than a month.");
            dialog.show(getFragmentManager(), "error");
            return false;
        }
        return true;
    }

    private void setDefaultDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, 1);
        defaultStartDate = calendar.getTime();
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        defaultEndDate = calendar.getTime();
    }

    private class UpdateInformationTask extends AsyncTask<String, Void, JSONArray> {
        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Loading reports ....");
            progressDialog.show();
        }

        @Override
        protected JSONArray doInBackground(String... params) {
            // Accept two date string as parameters.
            return CalorieDataManager.getReportByDateRange(params[0], params[1]);
        }

        @Override
        protected void onPostExecute(JSONArray jsonArray) {
            progressDialog.dismiss();
            LineGraphSeries<DataPoint> lineSeries = new LineGraphSeries<>();
            BarGraphSeries<DataPoint> barSeries = new BarGraphSeries<>();
            Bundle burnedBundle = new Bundle();
            Bundle consumedBundle = new Bundle();
            // Populate bundle for further query.
            for (int i = 0; i < jsonArray.length(); i ++) {
                try {
                    JSONObject report = jsonArray.getJSONObject(i);
                    double burned = report.getDouble("burned");
                    double consumed = report.getDouble("consumed");
                    String reportDate = TimeConvertion.convertToNormalDateFormat(report.getString("reportDate"));
                    burnedBundle.putDouble(reportDate, burned);
                    consumedBundle.putDouble(reportDate, consumed);
                } catch (JSONException e) {
                    Log.e("onParseReportJsonArray", e.getMessage(), e);
                }
            }
            Date startDate = TimeConvertion.parseNormalDateFormat(startDateView.getText().toString());
            Date endDate = TimeConvertion.parseNormalDateFormat(endDateView.getText().toString());
            if (startDate == null) {
                startDate = defaultStartDate;
            }
            if (endDate == null) {
                endDate = defaultEndDate;
            }
            Calendar startCalendar = Calendar.getInstance();
            startCalendar.setTime(startDate);
            Calendar endCalendar = Calendar.getInstance();
            endCalendar.setTime(endDate);
            double days = (endCalendar.getTimeInMillis() - startCalendar.getTimeInMillis()) / 24*60*60*1000;
            if (startCalendar.getTimeInMillis() == endCalendar.getTimeInMillis() || days < 3) {
                startCalendar.add(Calendar.DATE, -1);
                startDate = startCalendar.getTime();
                endCalendar.add(Calendar.DATE, 1);
                endDate = endCalendar.getTime();
            }
            do {
                // Test if returned report has the date specified.
                String currentDateString = TimeConvertion.getNormalDateFormatString(startCalendar.getTime());
                //Log.d("onReportAddData", currentDateString);
                if (burnedBundle.containsKey(currentDateString)) {
                    //Log.d("onReportAddData", "add existing data");
                    lineSeries.appendData(new DataPoint(startCalendar.getTime(), burnedBundle.getDouble(currentDateString)), true, 255);
                    // burned bundle is populated together with consumed bundle, so the consumed bundle has the same key too
                    barSeries.appendData(new DataPoint(startCalendar.getTime(), consumedBundle.getDouble(currentDateString)), true, 255);
                } else {
                    //Log.d("onReportAddData", "add 0.");
                    lineSeries.appendData(new DataPoint(startCalendar.getTime(), 0), true, 255);
                    barSeries.appendData(new DataPoint(startCalendar.getTime(), 0), true, 255);
                }
                // Add calendar by one day to generate missing reports.
                startCalendar.add(Calendar.DATE, 1);
            } while (startCalendar.getTimeInMillis() <= endCalendar.getTimeInMillis());


            // Set up the series properties
            //barSeries.setColor(getContext().getColor(R.color.ElementBackgroundColor));
            barSeries.setColor(Color.parseColor("#4487f2"));
            barSeries.setSpacing(30);
            lineSeries.setColor(Color.BLACK);
            lineSeries.setThickness(4);
            lineSeries.setDrawDataPoints(true);
            lineSeries.setDataPointsRadius(4);
            // Set up the x axis label
            graphView.removeAllSeries();
            graphView.addSeries(barSeries);
            graphView.addSeries(lineSeries);
            graphView.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(getActivity()));
            graphView.getGridLabelRenderer().setNumHorizontalLabels(3);
            graphView.getViewport().setMinX(startDate.getTime());
            graphView.getViewport().setMaxX(endDate.getTime());
            //graphView.getViewport().setMinY(0);
            graphView.getViewport().setMaxY(3000);
            graphView.getViewport().setXAxisBoundsManual(true);
        }
    }

}
