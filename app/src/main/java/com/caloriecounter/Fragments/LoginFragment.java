package com.caloriecounter.Fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.caloriecounter.Dialogs.SimpleInfoDialog;
import com.caloriecounter.MainActivity;
import com.caloriecounter.Objects.User;
import com.caloriecounter.R;
import com.caloriecounter.Utils.CalorieDataManager;
import com.caloriecounter.Utils.Database;
import com.caloriecounter.Utils.Login;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoginFragment extends Fragment {

    private TextView usernameView;
    private TextView passwordView;
    private ProgressDialog progressDialog;
    private final String LAST_LOGIN_ID = "last_login_id";
    private int lastLoginId;
    private boolean isSlientLogin = false;

    public LoginFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        Button loginButton = (Button) view.findViewById(R.id.loginButton);
        Button registerButton = (Button) view.findViewById(R.id.showRegisterFragmentButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLogin();
            }
        });
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showRegister(view);
            }
        });
        usernameView = (TextView) view.findViewById(R.id.username);
        passwordView = (TextView) view.findViewById(R.id.password);
        Bundle args = getArguments();
        if (args != null) {
            usernameView.setText(args.getString("username"));
            passwordView.setText(args.getString("password"));
            doLogin();
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        lastLoginId = sharedPreferences.getInt(LAST_LOGIN_ID, 0);
        if (lastLoginId != 0){
            Log.d("onLastLoginCheck", "Last Login user id is: " + Integer.toString(lastLoginId));
            doSilentLogin();
        }
        return view;
    }

    private void doLogin() {

        if (checkInfo(usernameView, passwordView)) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Logging in....");
            progressDialog.show();
            DoLoginAsync asyncTask = new DoLoginAsync();
            asyncTask.execute(usernameView.getText().toString(), passwordView.getText().toString());
        }
    }

    private void doSilentLogin() {
        User user = Database.getUserInfo(getActivity(), lastLoginId);
        DoLoginAsync asyncTask = new DoLoginAsync();
        isSlientLogin = true;
        asyncTask.execute(user.getUsername(), user.getPassword());
    }

    private boolean checkInfo(TextView usernameView, TextView passwordView) {
        boolean result = true;

        if (usernameView.length() == 0) {
            usernameView.setError(getString(R.string.emptyUsername));
            result = false;
        }
        if (passwordView.length() == 0) {
            passwordView.setError(getString(R.string.emptyPassword));
            result = false;
        }
        return result;
    }

    private void showRegister(View view) {
        Fragment registerFragment = new RegisterFragment();
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.loginActivityFrame, registerFragment).addToBackStack("loginFragment").commit();
    }

    private class DoLoginAsync extends AsyncTask<String, Void, Integer> {

        @Override
        protected Integer doInBackground(String... params) {
            User user = null;
            if (isSlientLogin)
                user = Login.doSlientLogin(params[0], params[1]);
            else
                user =  Login.doLogin(params[0], params[1]);
            if (user != null){
                Database.addUserRecord(getActivity(), user);
                User.setInstance(user);
                if (CalorieDataManager.fullyInitialiseUserData(getActivity()))
                    User.setIsFullyInitialised(true);
                return user.getUserId();
            }
            else{
                return null;
            }
        }

        @Override
        protected void onPostExecute(Integer userId) {
            if (userId != null) {
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                sharedPreferences.edit().putInt(LAST_LOGIN_ID, userId).apply();
                if (progressDialog != null)
                    progressDialog.dismiss();
                Intent mainScreenIntent = new Intent(getActivity(), MainActivity.class);
                mainScreenIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                mainScreenIntent.putExtra("userId", userId);
                startActivity(mainScreenIntent);
            }
            else
            {
                if (progressDialog != null)
                    progressDialog.dismiss();
                SimpleInfoDialog dialog = new SimpleInfoDialog();
                dialog.setTitle("Incorrect Information").setMessage("Username or password is wrong.");
                dialog.show(getFragmentManager(), "error");
            }
        }
    }

}
