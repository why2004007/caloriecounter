package com.caloriecounter.Dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.caloriecounter.Fragments.GoalFragment;
import com.caloriecounter.Fragments.MainScreenFragment;
import com.caloriecounter.Objects.User;
import com.caloriecounter.R;

/**
 * Created by Daniel on 15/09/2015.
 */
public class InputCustomWordsDialog extends DialogFragment {

    private EditText customWordsView;
    private MainScreenFragment.OnCustomWordSetListener onCustomWordSetListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_input_custom_words, null);
        customWordsView = (EditText) view.findViewById(R.id.dialogInputCustomWordsEditView);
        builder.setTitle("Set your custom words:").setView(view).
                setPositiveButton("Set", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (checkCustomWords()) {
                    SharedPreferences userSpecifiedPreference = getActivity().
                            getSharedPreferences(Integer.toString(User.getInstance().getUserId()), Activity.MODE_PRIVATE);
                    userSpecifiedPreference.edit().
                            putString("customWords", customWordsView.getText().toString()).apply();
                    if (onCustomWordSetListener != null)
                        onCustomWordSetListener.onSet();
                }
            }
        }).setNegativeButton("Cancel", null);
        return builder.create();
    }

    public boolean checkCustomWords() {
        if (customWordsView.getText().toString().trim().length() == 0) {
            SimpleInfoDialog dialog = new SimpleInfoDialog();
            dialog.setTitle("Incorrect Information").setMessage("You typed in nothing.");
            dialog.show(getFragmentManager(), "error");
            return false;
        }
        if (customWordsView.getText().toString().trim().length() > 60) {
            SimpleInfoDialog dialog = new SimpleInfoDialog();
            dialog.setTitle("Incorrect Information").setMessage("You can only set 100 characters.");
            dialog.show(getFragmentManager(), "error");
            return false;
        }
        return true;
    }

    public void setDialogOnQuitListener(MainScreenFragment.OnCustomWordSetListener listener) {
        this.onCustomWordSetListener = listener;
    }
}
