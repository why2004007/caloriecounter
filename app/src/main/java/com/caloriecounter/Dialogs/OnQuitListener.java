package com.caloriecounter.Dialogs;

import android.util.Log;

/**
 * Created by Daniel on 10/09/2015.
 */
public abstract class OnQuitListener {
        public abstract void onQuit();
}
