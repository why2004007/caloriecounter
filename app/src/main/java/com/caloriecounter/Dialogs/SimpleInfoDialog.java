package com.caloriecounter.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

/**
 * Created by Daniel on 7/09/2015.
 * Rewritten by myself, referencing google documents.
 */
public class SimpleInfoDialog extends DialogFragment {
    private String message;
    private String title;
    private boolean hasNegativeButton = false;
    private String positiveButtonText = "OK";
    private String negativeButtonText = "Cancel";

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title).setMessage(message);
        builder.setPositiveButton(positiveButtonText, null);
        if (hasNegativeButton)
            builder.setNegativeButton(negativeButtonText, null);
        return builder.create();
    }

    public String getMessage() {
        return message;
    }

    public SimpleInfoDialog setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public SimpleInfoDialog setTitle(String title) {
        this.title = title;
        return this;
    }

    public SimpleInfoDialog setHasNegativeButton(boolean hasNegativeButton) {
        this.hasNegativeButton = hasNegativeButton;
        return this;
    }


    public SimpleInfoDialog setPositiveButtonText(String positiveButtonText) {
        this.positiveButtonText = positiveButtonText;
        return this;
    }


    public SimpleInfoDialog setNegativeButtonText(String negativeButtonText) {
        this.negativeButtonText = negativeButtonText;
        return this;
    }
}