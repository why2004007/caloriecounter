package com.caloriecounter.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.caloriecounter.Fragments.GoalFragment;
import com.caloriecounter.Objects.User;
import com.caloriecounter.R;

/**
 * Created by Daniel on 8/09/2015.
 */

public class InputGoalDialog extends DialogFragment {

    private EditText goalView;
    private GoalFragment.DialogOnQuitListener dialogOnQuitListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_input_goal, null);
        goalView = (EditText) view.findViewById(R.id.dialogInputGoal);
        builder.setTitle("Set a new goal:").setView(view)
                .setPositiveButton("Set", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        User.getInstance().setGoal(Double.parseDouble(goalView.getText().toString()));
                        if (dialogOnQuitListener != null)
                            dialogOnQuitListener.onQuit();
                    }
                })
                .setNegativeButton("Cancel", null);
        return builder.create();
    }

    public void setDialogOnQuitListener(GoalFragment.DialogOnQuitListener dialogOnQuitListener) {
        this.dialogOnQuitListener = dialogOnQuitListener;
    }

}
