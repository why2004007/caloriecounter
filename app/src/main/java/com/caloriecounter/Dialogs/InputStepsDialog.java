package com.caloriecounter.Dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.caloriecounter.Fragments.GoalFragment;
import com.caloriecounter.Objects.User;
import com.caloriecounter.R;
import com.caloriecounter.Utils.CalorieDataManager;
import com.caloriecounter.Utils.Database;

/**
 * Created by Daniel on 10/09/2015.
 */
public class InputStepsDialog extends DialogFragment {

    private EditText stepsView;
    private Spinner stepUnitView;
    private OnQuitListener onQuitListener;
    private final double KM_TO_MILE = 0.621371192;
    private ProgressDialog progressDialog;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_input_step, null);
        stepsView = (EditText) view.findViewById(R.id.stepInputDialogStepsView);
        stepUnitView = (Spinner) view.findViewById(R.id.stepInputDialogStepUnitView);
        ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.stepsUnits, android.R.layout.simple_spinner_item);
        stepUnitView.setAdapter(spinnerAdapter);
        builder.setTitle("Add Steps Walked").setView(view)
                .setPositiveButton("Set", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int steps = getSteps();
                        //Log.d("t", Integer.toString(steps));
                        InsertStepDataTask task = new InsertStepDataTask();
                        task.execute(steps);
                        if (onQuitListener != null)
                            onQuitListener.onQuit();
                    }
                })
                .setNegativeButton("Cancel", null);
        return builder.create();
    }

    private int getSteps() {
        int stepUnitIndex = stepUnitView.getSelectedItemPosition();
        int steps = 0;
        if (stepUnitIndex == 0) {
            steps = (int) (Double.parseDouble(stepsView.getText().toString()) *
                    User.getInstance().getStepsPerMile());
        } else if (stepUnitIndex == 1) {
            steps = (int) (Double.parseDouble(stepsView.getText().toString()) * KM_TO_MILE
                    * User.getInstance().getStepsPerMile());
        } else {
            steps = (int) Double.parseDouble(stepsView.getText().toString());
        }
        return steps;
    }

    private class InsertStepDataTask extends AsyncTask<Integer, Void, Void> {

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Inserting data...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Integer... params) {
            Database.addStepsRecord(getActivity(), User.getInstance(), params[0]);
            CalorieDataManager.updateReports(getActivity(), false);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();
        }
    }

    public void setOnQuitListener(OnQuitListener listener) {
        this.onQuitListener = listener;
    }
}
