package com.caloriecounter.Config;

/**
 * Created by Daniel on 27/08/2015.
 */
public class Uris {

    //public final static String BASE_URI = "http://192.168.31.42:8080/CalorieCounterServer/webresources/caloriecounter.db.";
    public final static String BASE_URI = "http://118.139.18.56:8080/CalorieCounterServer/webresources/caloriecounter.db.";
    public final static String FIND_USER_BY_USERNAME_AND_PASSWORD = "users/findByUsernameANDPassword/%1$s/%2$s";
    public final static String GET_TOTAL_CONSUMED_BY_DATE = "foodintake/findByUserIdANDDateRange/%1$s/%2$s/%3$s";
    public final static String CHECK_USERNAME = "users/checkUsername/%1$s";
    //public final static String ADD_NEW_USER = "users/addNewUser/%1$s/%2$s/%3$s/%4$s/%5$s/%6$s/%7$s/%8$s/%9$s";
    public final static String ADD_NEW_USER = "users/";
    public final static String GET_BMR = "reports/getBMR/%1$s";
    public final static String GET_DAILY_BURNED = "reports/getDailyBurned/%1$s/%2$s";
    public final static String GET_CALORIE_PER_STEP = "reports/getCaloriePerStep/%1$s";
    public final static String GET_REPORT = "reports/findByReportDateANDUserid/%1$s/%2$s";
    public final static String GET_FOOD_LIST = "food/findByCategory/%1$s";
    public final static String ADD_FOOD_INTAKE = "foodintake";
    public final static String GET_FOOD_BY_ID = "food/findByFoodId/%1$s";
    public final static String GET_USER_BY_ID = "users/findByUserId/%1$s";
    public final static String CREATE_REPORT = "reports";
    public final static String SYNC_REPORT_PUT = "reports/%1$s";
    public final static String GET_REPORT_BY_DATE_RANGE = "reports/findByReportDateRangeANDUserid/%1$s/%2$s/%3$s";

}
