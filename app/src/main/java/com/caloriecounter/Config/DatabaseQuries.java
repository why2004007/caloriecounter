package com.caloriecounter.Config;

/**
 * Created by Daniel on 28/08/2015.
 */
public class DatabaseQuries {
        public static final String CREATE_TABLE_USER = "CREATE TABLE IF NOT EXISTS USER (" +
                "USER_ID INT NOT NULL PRIMARY KEY," +
                "USERNAME CHAR(20) NOT NULL," +
                "NICKNAME CHAR(20) NOT NULL," +
                "PASSWORD CHAR(32) NOT NULL," +
                "REG_DATE DATE NOT NULL," +
                "LATITUDE DOUBLE," +
                "LONGITUDE DOUBLE);";
        public static final String CREATE_TABLE_STEPS = "CREATE TABLE IF NOT EXISTS STEPS (" +
                "USER_ID INT NOT NULL," +
                "UPDATETIME DATETIME NOT NULL," +
                "STEPS INT NOT NULL," +
                "PRIMARY KEY (USER_ID, UPDATETIME)," +
                "FOREIGN KEY (USER_ID) REFERENCES USER (USER_ID));";
        public static final String INSERT_STEP_RECORD = "INSERT INTO STEPS (USER_ID, UPDATETIME, STEPS) VALUES (%1$s, DATETIME('%2$s'), %3$s)";
        public static final String INSERT_USER_RECORD = "INSERT INTO USER (USER_ID, USERNAME, " +
                "NICKNAME, PASSWORD, REG_DATE, LATITUDE, LONGITUDE) " +
                "VALUES (%1$s, '%2$s', '%3$s', '%4$s', Date(%5$s), '%6$s', %7$s)";
        public static final String CHECK_USER_EXISTENCE = "SELECT COUNT(*) FROM USER WHERE USER_ID = %1$s";
        public static final String SELECT_USER_BY_ID = "SELECT * FROM USER WHERE USER_ID = %1$s";
        public static final String GET_TOTAL_STEPS = "SELECT SUM(STEPS) FROM STEPS WHERE USER_ID = %1$s AND UPDATETIME BETWEEN DATE('%2$s') AND DATE('%3$s')";

}
