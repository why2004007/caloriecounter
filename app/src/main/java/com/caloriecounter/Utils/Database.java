package com.caloriecounter.Utils;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import com.caloriecounter.Config.DatabaseQuries;
import com.caloriecounter.Objects.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

/**
 * Created by Daniel on 28/08/2015.
 */
public class Database {

    private static class InitDb extends AsyncTask<Activity, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Activity... params) {
            Activity activity = params[0];
            SQLiteDatabase db = null;
            try {
                db = activity.openOrCreateDatabase("data.db", activity.MODE_PRIVATE, null);
                db.execSQL(DatabaseQuries.CREATE_TABLE_USER);
                db.execSQL(DatabaseQuries.CREATE_TABLE_STEPS);
            } catch (Exception e) {
                Log.e("OnInit", e.getMessage());
                Log.e("OnInit", e.getStackTrace().toString());
            } finally {
                db.close();
            }
            Log.i("onInitDb", "DB initialized successfully.");
            return false;
        }
    }

    public static void initDb(Activity activity) {
        AsyncTask task = new InitDb();
        task.execute(new Activity[]{activity});
    }

    public static boolean addStepsRecord(Activity activity, User user, int steps) {
        SQLiteDatabase db = null;
        try {
            db = activity.openOrCreateDatabase("data.db", Activity.MODE_PRIVATE, null);
            //Log.d("t", String.format(DatabaseQuries.INSERT_STEP_RECORD, user.getUserId(),
            //        TimeConvertion.getNormalDateTimeString(new Date()), steps));
            db.execSQL(String.format(DatabaseQuries.INSERT_STEP_RECORD, user.getUserId(),
                    TimeConvertion.getNormalDateTimeString(new Date()), steps));
        } catch (Exception e) {
            Log.e("OnAddStepRecord", e.getMessage());
            return false;
        } finally {
            db.close();
        }
        return true;
    }

    public static boolean addUserRecord(Activity activity, User user) {
        SQLiteDatabase db = null;
        try {
            db = activity.openOrCreateDatabase("data.db", Activity.MODE_PRIVATE, null);
            Cursor c = db.rawQuery(String.format(DatabaseQuries.CHECK_USER_EXISTENCE, user.getUserId()), null);
            c.moveToFirst();
            Log.d("test",Integer.toString(c.getColumnCount()));
            if (c.getInt(0) == 0 ) {
                Log.d("onAddNewUser", "User record does not exist, add new user");
                // Parameter list:
                // USER_ID, USERNAME, NICKNAME, PASSWORD, REG_DATE, LATITUDE, LONGITUDE
                db.execSQL(String.format(DatabaseQuries.INSERT_USER_RECORD, user.getUserId(),
                        user.getUsername().toUpperCase(), user.getNickname(), user.getPassword(),
                        TimeConvertion.getNormalDateFormatString(user.getRegDate()), -37.876470, 145.044078));
            }
            c.close();
        } catch (Exception e) {
            Log.e("OnAddUserRecord", e.getMessage());
            return false;
        } finally {
            db.close();
        }
        return true;
    }

    public static User getUserInfo(Activity activity, int userId) {
        SQLiteDatabase db = null;
        User user = new User();
        try {
            db = activity.openOrCreateDatabase("data.db", activity.MODE_PRIVATE, null);
            //Log.d("onQueryUserInfo","SQL Statement: " + String.format(DatabaseQuries.SELECT_USER_BY_ID, userId));
            Cursor c = db.rawQuery(String.format(DatabaseQuries.SELECT_USER_BY_ID, userId), null);
            if (c.moveToFirst()) {
                Log.d("columnHeader", c.getColumnName(0));
                user.setUserId(Integer.parseInt(c.getString(c.getColumnIndex("USER_ID"))));
                user.setUsername(c.getString(c.getColumnIndex("USERNAME")));
                user.setNickname(c.getString(c.getColumnIndex("NICKNAME")));
                user.setPassword(c.getString(c.getColumnIndex("PASSWORD")));
                user.setLatitude(Double.parseDouble(c.getString(c.getColumnIndex("LATITUDE"))));
                user.setLongitude(Double.parseDouble(c.getString(c.getColumnIndex("LONGITUDE"))));
                c.close();
            }
            else {
                c.close();
                Log.e("onQueryUserInfo","Cusor is null.");
                return null;
            }
        } catch (Exception e) {
            Log.e("OnQueryUserInfo", e.getMessage(), e);
            return null;
        } finally {
            db.close();
        }
        return user;
    }

    public static int getTotalSteps(Activity activity, User user, Date start, Date end) {
        // YYYY-MM-DD HH:MM:SS.SSS Format that can be converted by SQLite3
        int miliSecondsPerDay = 24 * 60 * 60 * 1000;
        end = new Date(end.getTime() + miliSecondsPerDay - 1);
        int totalStep = 0;
        SQLiteDatabase db = null;
        try {
            db = activity.openOrCreateDatabase("data.db", Activity.MODE_PRIVATE, null);
            Cursor c = db.rawQuery(String.format(DatabaseQuries.GET_TOTAL_STEPS,user.getUserId(), TimeConvertion.getNormalDateTimeString(start), TimeConvertion.getNormalDateTimeString(end)), null);
            if (c.moveToFirst()) {
                totalStep = c.getInt(0);
            }
            else {
                Log.e("onQueryTotalSteps","Cursor points to nothing.");
            }
        } catch (Exception e) {
            Log.e("onQueryTotalSteps", e.getMessage(), e);
            return 0;
        } finally {
            db.close();
        }
        return totalStep;
    }
}
