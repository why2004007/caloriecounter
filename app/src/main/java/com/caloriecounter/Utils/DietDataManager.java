package com.caloriecounter.Utils;

import android.util.Log;

import com.caloriecounter.Config.Uris;
import com.caloriecounter.Objects.Food;
import com.caloriecounter.Objects.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Daniel on 10/09/2015.
 */
public class DietDataManager {

    public static ArrayList<Food> getFoodList(String category) {
        String url = String.format(Uris.BASE_URI + Uris.GET_FOOD_LIST, category);
        HttpConnection connection = new HttpConnection(url);
        connection.GET();
        if (connection.isSucceed()) {
            try {
                JSONArray foodList = new JSONArray(connection.getResponse());
                ArrayList<Food> foodItemList = new ArrayList<Food>();
                for(int i = 0; i < foodList.length(); i++) {
                    JSONObject foodItem = foodList.getJSONObject(i);
                    foodItemList.add(new Food(foodItem.getInt("foodId"),
                            foodItem.getString("foodName"),
                            foodItem.getString("serveDetail")));
                }
                return foodItemList;
            } catch (JSONException e) {
                Log.e("onParseFoodList", e.getMessage(), e);
                return null;
            }
        } else {
            Log.e("onConnectToServer", connection.getErrorMessage());
            return null;
        }
    }

    public static boolean addConsumedRecord(Integer foodId, Double quantity) {
        JSONObject user = null;
        JSONObject food = null;
        HttpConnection connection = new HttpConnection(
                String.format(Uris.BASE_URI + Uris.GET_USER_BY_ID, User.getInstance().getUserId()));
        connection.GET();
        if (connection.isSucceed()) {
            try {
                user = new JSONObject(connection.getResponse());
            } catch (JSONException e) {
                Log.e("onParseUser", e.getMessage(), e);
            }
        } else {
            return false;
        }
        connection = new HttpConnection(String.format(Uris.BASE_URI + Uris.GET_FOOD_BY_ID, foodId));
        connection.GET();
        if (connection.isSucceed()) {
            try {
                food = new JSONObject(connection.getResponse());
            } catch (JSONException e) {
                Log.e("onParseUser", e.getMessage(), e);
            }
        } else {
            return false;
        }
        JSONObject record = new JSONObject();
        try {
            record.put("foodId", food);
            record.put("userId", user);
            record.put("intakeTime", TimeConvertion.getJSONDateFormatString(new Date()));
            record.put("quantity", quantity);
        } catch (JSONException e) {
            Log.e("onCreateRecord", e.getMessage(),e);
        }
        connection = new HttpConnection(Uris.BASE_URI + Uris.ADD_FOOD_INTAKE);
        connection.POST(record.toString());
        return connection.isSucceed();
    }

}
