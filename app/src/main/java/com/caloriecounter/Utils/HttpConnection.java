package com.caloriecounter.Utils;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Daniel on 28/08/2015.
 */
public class HttpConnection {
    private URL url;
    private String errorMessage;
    private boolean error = false;
    private String response;
    private HttpURLConnection urlConnection;


    public HttpConnection() {

    }

    public HttpConnection(String url) {
        try {
            this.url = new URL(url);
        } catch (MalformedURLException e) {
            Log.e("onCreateConnection", e.getMessage(),e);
            this.error = true;
            this.errorMessage = "Malformed Url Provided";
        }
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public InputStream streamGET() {
        if (!error) {
            StringBuilder builder = new StringBuilder();
            Log.d("onConnect","url to connect:\n" + this.url.toString());
            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("Accept", "application/json");
                return urlConnection.getInputStream();
            } catch (IOException e) {
                errorMessage = e.getMessage();
                Log.e("onGET", e.getMessage(), e);
                error = true;
                return null;
            }
        } else
            return null;
    }

    public void GET() {
        if (!error) {
            StringBuilder builder = new StringBuilder();
            Log.d("onConnect","url to connect:\n" + this.url.toString());
            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestProperty("Accept", "application/json");
                BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String line = "";
                boolean flag = true;
                while (flag) {
                    line = reader.readLine();
                    if (line != null) {
                        builder.append(line);
                    } else {
                        flag = false;
                    }
                }
                response = builder.toString();
            } catch (IOException e) {
                errorMessage = e.getMessage();
                Log.e("onGET", e.getMessage(), e);
                error = true;
            } finally {
                urlConnection.disconnect();
            }
        }
    }

    public void POST(String data) {
        if (!error) {
            Log.d("postData",data);
            StringBuilder builder = new StringBuilder();
            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type","application/json");
                urlConnection.setRequestProperty("Accept", "application/json");
                urlConnection.setDoOutput(true);
                urlConnection.setChunkedStreamingMode(0);
                OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                out.write(data.getBytes());
                out.flush();
                out.close();
                BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String line = "";
                boolean flag = true;
                while (flag) {
                    line = reader.readLine();
                    if (line != null) {
                        builder.append(line);
                    } else {
                        flag = false;
                    }
                }
                response = builder.toString();
            } catch (IOException e) {
                errorMessage = e.getMessage();
                Log.e("onPOST", e.getMessage(), e);
                error = true;
            } finally {
                urlConnection.disconnect();
            }
        }
    }

    public void DELETE() {
        if (!error) {
            StringBuilder builder = new StringBuilder();
            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("DELETE");
            } catch (IOException e) {
                errorMessage = e.getMessage();
                Log.e("onDELETE", e.getMessage(), e);
                error = true;
            } finally {
                urlConnection.disconnect();
            }
        }
    }

    public void PUT(String data) {
        if (!error) {
            StringBuilder builder = new StringBuilder();
            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("PUT");
                urlConnection.setRequestProperty("Content-Type","application/json");
                urlConnection.setDoOutput(true);
                urlConnection.setChunkedStreamingMode(0);
                OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
                out.write(data.getBytes());
                out.close();
                BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                String line = "";
                boolean flag = true;
                while (flag) {
                    line = reader.readLine();
                    if (line != null) {
                        builder.append(line);
                    } else {
                        flag = false;
                    }
                }
                response = builder.toString();
            } catch (IOException e) {
                errorMessage = e.getMessage();
                Log.e("onPUT", e.getMessage(), e);
                error = true;
            } finally {
                urlConnection.disconnect();
            }
        }
    }

    public boolean isSucceed() {
        return ! error;
    }
}
