package com.caloriecounter.Utils;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Daniel on 13/09/2015.
 */
public class NationalNutrientDatabase {

    private final static String GET_NDBNO = "http://api.nal.usda.gov/ndb/search/?format=json&q=%1$s&sort=n&max=25&offset=0&api_key=%2$s";
    private final static String GET_NUTRIENTS = " http://api.nal.usda.gov/ndb/nutrients/?format=json&api_key=%1$s%2$s&ndbno=%3$s";
    private static final String NATIONAL_NUTRIENT_API_KEY = "AxtWxX53NMGRQZbnSV8V2TwEGCg2R3P5LRVTVH3H";
    public final static String ENERGY_ID = "208";
    public final static String PROTEIN_ID = "203";
    public final static String FAT_ID = "204";
    public final static String CARBOHYDRATE = "205";
    public final static String SODIUM = "307";
    public static int nextItemId = 0;

    public static Bundle getNutrients(String foodName, boolean isFailed) {
        if (isFailed) {
            nextItemId += 1;
        } else {
            nextItemId = 0;
        }
        String ndbno = getNdbno(foodName);
        if (ndbno != null) {
            String url = String.format(GET_NUTRIENTS, NATIONAL_NUTRIENT_API_KEY, getNutrientsParemeters(), ndbno);
            HttpConnection connection = new HttpConnection(url);
            connection.GET();
            if (connection.isSucceed()) {
                try {
                    Bundle nutrientInfo = new Bundle();
                    JSONObject result = new JSONObject(connection.getResponse());
                    JSONObject report = result.getJSONObject("report");
                    JSONArray foodList = report.getJSONArray("foods");
                    JSONObject food = foodList.getJSONObject(0);
                    JSONArray nutrients = food.getJSONArray("nutrients");
                    for (int i = 0; i < nutrients.length(); i++) {
                        JSONObject nutrient = nutrients.getJSONObject(i);
                        String nutrientCategory = null;
                        switch (nutrient.getString("nutrient_id")) {
                            case ENERGY_ID: nutrientCategory = "energy"; break;
                            case PROTEIN_ID: nutrientCategory = "protein"; break;
                            case FAT_ID: nutrientCategory = "fat"; break;
                            case CARBOHYDRATE: nutrientCategory = "carbohydrate"; break;
                            case SODIUM: nutrientCategory = "sodium"; break;
                        }
                        nutrientInfo.putString(nutrientCategory, nutrient.getString("value") +
                                nutrient.getString("unit"));
                    }
                    return nutrientInfo;
                } catch (JSONException e) {
                    Log.e("onParseFoodNutrients", e.getMessage(), e);
                    return null;
                } catch (NullPointerException e) {
                    Log.e("onParseFoodNutrients", e.getMessage(), e);
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public static String getNdbno(String foodName) {
        String url = String.format(GET_NDBNO, foodName, NATIONAL_NUTRIENT_API_KEY);
        HttpConnection connection = new HttpConnection(url);
        connection.GET();
        if (connection.isSucceed()) {
            try {
                JSONObject result = new JSONObject(connection.getResponse());
                JSONObject list = result.getJSONObject("list");
                JSONArray items = list.getJSONArray("item");
                if (items.length() != 0 && items.length() > nextItemId) {
                    JSONObject food = items.getJSONObject(nextItemId);
                    return food.getString("ndbno");
                } else {
                    return null;
                }
            } catch (JSONException e) {
                Log.e("onParseNDBJSON", e.getMessage(), e);
                return null;
            }
        } else {
            Log.e("onGetNBDNO", "Connection Failed.");
            return null;
        }
    }

    public static String getNutrientsParemeters() {
        String parameterTemplate = "&nutrients=%1$s";
        StringBuilder builder = new StringBuilder();
        builder.append(String.format(parameterTemplate, ENERGY_ID));
        builder.append(String.format(parameterTemplate, PROTEIN_ID));
        builder.append(String.format(parameterTemplate, FAT_ID));
        builder.append(String.format(parameterTemplate, CARBOHYDRATE));
        builder.append(String.format(parameterTemplate, SODIUM));
        return builder.toString();
    }
}
