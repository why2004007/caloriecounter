package com.caloriecounter.Utils;

import android.util.Log;

import com.caloriecounter.Config.Uris;
import com.caloriecounter.Objects.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Daniel on 28/08/2015.
 */
public class Register {
    public Register() {

    }

    public static Boolean checkUsernameValid(String username) {
        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(Uris.BASE_URI).append(Uris.CHECK_USERNAME);
        HttpConnection conn = new HttpConnection(String.format(urlBuilder.toString(), username));
        conn.GET();
        if (conn.isSucceed()) {
            String response = conn.getResponse();
            if (response.equals("true"))
                return true;
            else
                return false;
        } else {
            Log.e("onPostConnected", conn.getErrorMessage());
            return null;
        }
    }

    public static boolean doRegister(String username, String password, String nickname, String gender, String dob, String height, String weight, String stepsPerMile, String activityLevel) {
        JSONObject jObj = new JSONObject();
        password = Encryption.encryptPassword(password);
        //{,"regDate":"1990-08-17T00:00:00+10:00","stepsPerMile":2200,"userId":27,"username":"billgates11","weight":70.0}

        try {
            jObj.put("activityLevel", Integer.parseInt(activityLevel));
            jObj.put("dob", TimeConvertion.convertToJSONDateFormat(dob));
            jObj.put("gender", gender.toUpperCase());
            jObj.put("height", Double.parseDouble(height));
            jObj.put("nickname", nickname);
            jObj.put("password", password.toUpperCase());
            jObj.put("regDate", TimeConvertion.getJSONDateFormatString(new Date()));
            jObj.put("stepsPerMile", Integer.parseInt(stepsPerMile));
            jObj.put("username", username);
            jObj.put("weight", Double.parseDouble(weight));
        } catch (JSONException e) {
            Log.e("onCreateRJSONObject", e.getMessage(), e);
        }

        HttpConnection conn = new HttpConnection(Uris.BASE_URI + Uris.ADD_NEW_USER);
        conn.POST(jObj.toString());
        if (conn.isSucceed()) {
            conn = new HttpConnection(String.format(Uris.BASE_URI + Uris.FIND_USER_BY_USERNAME_AND_PASSWORD, username, password.toUpperCase()));
            conn.GET();
            if (conn.isSucceed()) {
                if (!conn.getResponse().equals("[]")) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            Log.e("onPostConnected", conn.getErrorMessage());
            return false;
        }
    }
}
