package com.caloriecounter.Utils;

import android.util.Xml;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Daniel on 27/08/2015.
 */
public class Encryption {

    private static final String SECRET_KEY = "9x9Am*kN7us09";

    public Encryption() {

    }
    /*
    * Find the code at RFC document and Oracle java documents,
    * modified by myself.
    * */
    public static String encryptPassword(String password) {
        MessageDigest digester = null;
        try {
            digester = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            Logger.getAnonymousLogger().log(Level.WARNING, "Can not find md5 algorithm.");
        }
        digester.update(password.getBytes());
        byte[] hashArray = digester.digest(SECRET_KEY.getBytes());
        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < hashArray.length; i++) {
            if ((hashArray[i] & 0xff) < 0x10)
                hexString.append("0" + Integer.toHexString(hashArray[i] & 0xff));
            else
                hexString.append(Integer.toHexString(hashArray[i] & 0xff));
        }
        return hexString.toString();
    }
}
