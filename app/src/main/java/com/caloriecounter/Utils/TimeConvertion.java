package com.caloriecounter.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Daniel on 11/09/2015.
 */
public class TimeConvertion {

    private static final SimpleDateFormat normalDateFormat = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat normalDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    private static final SimpleDateFormat jsonDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    public static Date getDate(Integer year, Integer month, Integer day) {
        try {
            return normalDateFormat.parse(year.toString() + "-" + month.toString() + "-" + day.toString());
        } catch (ParseException e) {
            return null;
        }
    }

    public static String getNormalDateFormatString(Date date) {
        return normalDateFormat.format(date);
    }

    public static String getJSONDateFormatString(Date date) {
        StringBuilder builder = new StringBuilder(jsonDateFormat.format(date));
        builder.insert(builder.length() - 2, ":");
        return builder.toString();
    }

    public static Date parseNormalDateFormat(String date) {
        try {
            return normalDateFormat.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static Date parseJSONDateFormat(String date) {
        try {
            return jsonDateFormat.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    public static String convertToJSONDateFormat(String date) {
        Date temp = parseNormalDateFormat(date);
        if (temp != null) {
            return getJSONDateFormatString(temp);
        } else {
            return null;
        }
    }

    public static String convertToNormalDateFormat(String date) {
        Date temp = parseJSONDateFormat(date);
        if (temp != null) {
            return getNormalDateFormatString(temp);
        } else {
            return null;
        }
    }

    public static String getNormalDateTimeString(Date date) {
        return normalDateTimeFormat.format(date);
    }
}
