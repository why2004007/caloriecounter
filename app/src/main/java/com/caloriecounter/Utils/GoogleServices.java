package com.caloriecounter.Utils;

import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Daniel on 11/09/2015.
 * These codes are written by reviewing the google api.
 * Due to the incomplete support of map search from mapquest, (Australia does not have fully geo-info)
 * The parks are retrieved by google places service.
 */
public class GoogleServices {

    private final static String GOOGLE_CUSTOM_SEARCH_API = "https://www.googleapis.com/customsearch/v1?q=%1$s&cx=%2$s&key=%3$s";
    private static final String GOOGLE_SEARCH_ENGINE_API_KEY ="AIzaSyCzAuR4_2fT3pVOQQYu-PA6ELp89xcsC-I";
    private static final String GOOGLE_SEARCH_ENGINE_ID ="010036271671186228397%3A2umpwyv7-4m";
    private static final String GOOGLE_PLACES_API_KEY = "AIzaSyApQyjwLMvVRxUeJzyTe1VKQtu4qVSZpUw";
    private static final String GOOGLE_PLACES_API = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%1$s,%2$s&radius=%3$s&types=%4$s&key=%5$s";
    private static final String GOOGLE_PLACES_PAGE_TOKEN_URI = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?pagetoken=%1$s&key=%2$s";


    public static Bundle searchWeb(String keywords) {
        Bundle result = new Bundle();
        String searchUrl = String.format(GOOGLE_CUSTOM_SEARCH_API, keywords,
                GOOGLE_SEARCH_ENGINE_ID, GOOGLE_SEARCH_ENGINE_API_KEY);
        HttpConnection connection = new HttpConnection(searchUrl);
        connection.GET();
        if (connection.isSucceed()) {
            try {
                JSONObject jObj = new JSONObject(connection.getResponse());
                JSONObject firstResult = jObj.getJSONArray("items").getJSONObject(0);
                result.putString("description",firstResult.getString("snippet"));result.putString("Link", firstResult.getString("link"));
                try {
                    int i = 0;
                    do {
                        if (jObj.getJSONArray("items").length() <= i) {
                            break;
                        }
                        firstResult = jObj.getJSONArray("items").getJSONObject(i);
                        if (firstResult.has("pagemap")) {
                            String imageUrl = firstResult.getJSONObject("pagemap").getJSONArray("cse_image").getJSONObject(0).getString("src");
                            result.putString("imageLink", imageUrl);
                        } else {
                            i += 1;
                        }
                    }while (!firstResult.has("pagemap"));
                } catch (NullPointerException e) {
                    result.putString("imageLink", null);
                }
                return result;
            } catch (JSONException e) {
                Log.e("onParseGoogleSearch", e.getMessage(), e);
                return null;
            }
        } else {
            return null;
        }
    }

    public static JSONArray searchPlaces(String type, Integer radius, Double latitude, Double longitude) {
        JSONArray result = new JSONArray();

        JSONObject placesJSON = null;
        String nextPageToken = null;
        String searchUrl = null;
        boolean isFirstTime = true;
        int pages = 0;
        try {
            // Repeatly retrieve places information until reach the end.
            do {
                if (isFirstTime) {
                    searchUrl = String.format(GOOGLE_PLACES_API, latitude.toString(), longitude.toString(),
                            radius.toString(), type, GOOGLE_PLACES_API_KEY);
                    isFirstTime = false;
                } else {
                    searchUrl = String.format(GOOGLE_PLACES_PAGE_TOKEN_URI, nextPageToken,
                            GOOGLE_PLACES_API_KEY);
                }
                HttpConnection connection = new HttpConnection(searchUrl);
                connection.GET();
                if (connection.isSucceed()) {
                    Log.d("test", connection.getResponse());
                    placesJSON = new JSONObject(connection.getResponse());
                    if (placesJSON.has("next_page_token"))
                        nextPageToken = placesJSON.getString("next_page_token");
                    else
                        nextPageToken = null;
                    if (placesJSON.getString("status").equals("OK")) {
                        JSONArray resultList = placesJSON.getJSONArray("results");
                        if (resultList != null) {
                            for (int i = 0; i < resultList.length(); i++) {
                                result.put(resultList.get(i));
                            }
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                } else {
                    if (isFirstTime)
                        return result;
                    else
                        return null;
                }
                pages += 1;
                SystemClock.sleep(2000);
            } while (nextPageToken != null && pages < 4);
            return result;
        } catch (JSONException e) {
            Log.e("onParsePlacesJSON", e.getMessage(), e);
            return null;
        }
    }

}
