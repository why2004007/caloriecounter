package com.caloriecounter.Utils;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.caloriecounter.Config.Uris;
import com.caloriecounter.Objects.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Daniel on 7/09/2015.
 */
public class CalorieDataManager {

    private static final int TOTAL_SECONDS_PER_DAY = 24 * 60 * 60;
    private static final int MAX_RETRY_TIMES = 4;


    public static boolean fullyInitialiseUserData(Activity activity) {
        int retry = 0;
        while (! getBmr()) {
            if (retry > MAX_RETRY_TIMES)
                return false;
            retry += 1;
        }
        retry = 0;
        while (! getCaloriePerStep()) {
            if (retry > MAX_RETRY_TIMES)
                return false;
            retry += 1;
        }
        retry = 0;
        while (! getDailyBurned()) {
            if (retry > MAX_RETRY_TIMES)
                return false;
            retry += 1;
        }
        while (! getGoal()) {
            if (retry > MAX_RETRY_TIMES)
                return false;
            retry += 1;
        }
        return true;
    }

    public static boolean getBmr() {
        String url = Uris.BASE_URI + Uris.GET_BMR;
        String response = getData(String.format(url, User.getInstance().getUserId()));
        if (response != null) {
            User.getInstance().setBmr(Double.parseDouble(response));
            return true;
        }
        return false;
    }

    // This method cannot be called before bmr has already been initialised.
    private static boolean getDailyBurned() {
        String url = Uris.BASE_URI + Uris.GET_DAILY_BURNED;
        String response = getData(String.format(url, User.getInstance().getUserId(), User.getInstance().getBmr()));
        if (response != null) {
            User.getInstance().setDailyBurned(Double.parseDouble(response));
            return true;
        }
        return false;
    }

    private static boolean getCaloriePerStep() {
        String url = Uris.BASE_URI + Uris.GET_CALORIE_PER_STEP;
        String response = getData(String.format(url, User.getInstance().getUserId()));
        if (response != null) {
            User.getInstance().setCaloriePerStep(Double.parseDouble(response));
            return true;
        }
        return false;
    }

    public static boolean getGoal() {
        String url = Uris.BASE_URI + Uris.GET_REPORT;
        String response = getData(String.format(url, TimeConvertion.getNormalDateFormatString(new Date()), User.getInstance().getUserId()));
        if (response != null) {
            try {
                if (response.length() != 0) {
                    JSONObject jObj = new JSONObject(response);
                    double goal = jObj.getDouble("goal");
                    User.getInstance().setGoal(goal);
                }
            } catch (JSONException e) {
                Log.e("onParseGoalObject", e.getMessage(), e);
                return false;
            }
            return true;
        }
        User.getInstance().setGoal(0);
        return true;
    }

    private static String getData(String url) {

        HttpConnection conn = new HttpConnection(url);
        conn.GET();
        if (conn.isSucceed())
            return conn.getResponse();
        else
            return null;
    }

    public static double getTotalBurned(Activity activity) {
        return getCurrentDailyBurned() + getBurnedByStep(activity);
    }

    public static double getCurrentDailyBurned() {
        User user = User.getInstance();
        Double dailyBurned = user.getDailyBurned();
        Calendar calendar = Calendar.getInstance();
        double passedTime = calendar.get(Calendar.HOUR_OF_DAY) * 3600 +
                calendar.get(Calendar.MINUTE) * 60 + calendar.get(Calendar.SECOND);
        return passedTime / TOTAL_SECONDS_PER_DAY * dailyBurned;
    }

    public static double getBurnedByStep(Activity activity) {
        User user = User.getInstance();
        int steps = Database.getTotalSteps(activity, user, new Date(), new Date());
        return user.getCaloriePerStep() * steps;
    }

    public static double getTotalConsumed() {
        String currentDate = TimeConvertion.getNormalDateFormatString(new Date());
        String url = String.format(Uris.BASE_URI + Uris.GET_TOTAL_CONSUMED_BY_DATE, User.getInstance().getUserId(), currentDate, currentDate);
        HttpConnection connection = new HttpConnection(url);
        connection.GET();
        double totalConsumed = 0;
        if (connection.isSucceed()) {
            try {
                JSONArray recordList = new JSONArray(connection.getResponse());
                for (int i = 0; i < recordList.length(); i ++) {
                    JSONObject record = recordList.getJSONObject(i);
                    JSONObject food = record.getJSONObject("foodId");
                    double amount = record.getDouble("quantity");
                    double calorie = food.getDouble("calorie");
                    totalConsumed += amount * calorie;
                }
            } catch (JSONException e) {
                Log.e("onParseConsumedRecords", e.getMessage(), e);
            }
        }
        return totalConsumed;
    }

    public static double getRemaining(Activity activity) {
        return User.getInstance().getGoal() + getTotalConsumed()
                - User.getInstance().getDailyBurned() - getBurnedByStep(activity);
    }

    public static JSONArray getReportByDateRange(String startDate, String endDate) {
        String url = Uris.BASE_URI + Uris.GET_REPORT_BY_DATE_RANGE;
        String response = getData(String.format(url, User.getInstance().getUserId(), startDate, endDate));
        Bundle result = new Bundle();
        if (response != null) {
            try {
                return new JSONArray(response);
            } catch (JSONException e) {
                Log.e("onParseReportJsonArray", "Parse JSON array failed.");
                return null;
            }
        } else {
            return null;
        }
    }

    public static JSONObject getUser(int userId) {
        String url = String.format(Uris.BASE_URI + Uris.GET_USER_BY_ID, userId);
        HttpConnection connection = new HttpConnection(url);
        connection.GET();
        if (connection.isSucceed()) {
            try {
                return new JSONArray(connection.getResponse()).getJSONObject(0);
            } catch (JSONException e) {
                Log.e("onParseUserJson", e.getMessage(), e);
                return null;
            }
        } else {
            Log.e("onGetUserJson", connection.getErrorMessage());
            return null;
        }
    }

    public static boolean updateReports(Activity activity, boolean updateGoal) {
        String url = String.format(Uris.BASE_URI + Uris.GET_REPORT,
                TimeConvertion.getNormalDateFormatString(new Date()), User.getInstance().getUserId());
        // This needs to be formatted.
        String updateUrl = Uris.BASE_URI + Uris.SYNC_REPORT_PUT;
        // This doesn't.
        String createUrl = Uris.BASE_URI + Uris.CREATE_REPORT;
        HttpConnection connection = new HttpConnection(url);
        connection.GET();
        if (connection.isSucceed()) {
            try {
                if (connection.getResponse().length() != 0) {
                    JSONObject report = new JSONObject(connection.getResponse());
                    double totalBurned = getTotalBurned(activity);
                    report.put("burned", totalBurned);
                    double totalConsumed = getTotalConsumed();
                    report.put("consumed", totalConsumed);
                    Date today = new Date();
                    int steps = Database.getTotalSteps(activity, User.getInstance(), today, today);
                    report.put("steps", steps);
                    if (updateGoal) {
                        report.put("goal", User.getInstance().getGoal());
                        double remaining = User.getInstance().getGoal() - getTotalBurned(activity);
                        if (remaining > 0) {
                            report.put("remaining", remaining);
                        }
                    }
                    updateUrl = String.format(updateUrl, report.getInt("reportId"));
                    HttpConnection updateConnection = new HttpConnection(updateUrl);
                    updateConnection.PUT(report.toString());
                    return true;
                } else {
                    JSONObject report = new JSONObject();
                    JSONObject user = getUser(User.getInstance().getUserId());
                    if (user != null) {
                        report.put("userId", user);
                        double totalBurned = getTotalBurned(activity);
                        report.put("burned", totalBurned);
                        double totalConsumed = getTotalConsumed();
                        report.put("consumed", totalConsumed);
                        Date today = new Date();
                        int steps = Database.getTotalSteps(activity, User.getInstance(), today, today);
                        report.put("steps", steps);
                        if (updateGoal) {
                            report.put("goal", User.getInstance().getGoal());
                            double remaining = User.getInstance().getGoal() - getTotalBurned(activity);
                            if (remaining > 0) {
                                report.put("remaining", remaining);
                            }
                        } else {
                            report.put("goal", 0.0);
                            report.put("remaining", 0.0);
                        }
                        HttpConnection createConnection = new HttpConnection(createUrl);
                        createConnection.POST(report.toString());
                        return true;
                    } else {
                        return false;
                    }
                }
            } catch (JSONException e) {
                Log.e("onUpdateReports", e.getMessage(), e);
                return false;
            }

        } else {
            Log.e("onUpdateReports", connection.getErrorMessage());
            return false;
        }
    }
}
