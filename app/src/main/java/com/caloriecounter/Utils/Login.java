package com.caloriecounter.Utils;

import android.util.Log;

import com.caloriecounter.Config.Uris;
import com.caloriecounter.Objects.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Daniel on 27/08/2015.
 */
public class Login {

    public Login() {

    }

    public static User doLogin(String username, String password) {
        String encryptedPassword = Encryption.encryptPassword(password);
        return doSlientLogin(username, encryptedPassword);
    }

    public static User doSlientLogin(String username, String password) {
        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append(Uris.BASE_URI).append(Uris.FIND_USER_BY_USERNAME_AND_PASSWORD);
        User user = new User();
        String jsonString = "";
        HttpConnection conn = new HttpConnection(String.format(urlBuilder.toString(), username, password));
        conn.GET();
        JSONObject jObj;
        if (conn.isSucceed()){
            jsonString = conn.getResponse();
            try {
                Log.d("jsonString",jsonString);
                JSONArray jsonArray = new JSONArray(jsonString);
                if (jsonArray.length() == 0){
                    return null;
                }
                else{
                    jObj = (JSONObject) jsonArray.get(0);
                }
                // sample json : {"activityLevel":1,"dob":"1988-03-18T00:00:00+11:00","gender":"M","height":178.0,"nickname":"Bill","password":"Bg123456","stepsPerMile":2200,"userId":1,"username":"billgates","weight":70.0}
                user.setUserId(jObj.getInt("userId"));
                user.setUsername(username);
                user.setPassword(password);
                user.setNickname(jObj.getString("nickname"));
                user.setGender(jObj.getString("gender"));
                user.setHeight(jObj.getDouble("height"));
                user.setWeight(jObj.getDouble("weight"));
                user.setDob(TimeConvertion.parseJSONDateFormat(jObj.getString("dob")));
                user.setRegDate(TimeConvertion.parseJSONDateFormat(jObj.getString("regDate")));
                user.setStepsPerMile(jObj.getInt("stepsPerMile"));
                user.setActivityLevel(jObj.getInt("activityLevel"));
            } catch (JSONException e) {
                Log.e("onParseJson", e.getMessage());
                return null;
            }
        }
        else {
            Log.e("onReceivingUserJson", conn.getErrorMessage());
            return null;
        }
        return user;
    }
}
