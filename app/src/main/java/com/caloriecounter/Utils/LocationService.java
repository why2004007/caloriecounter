package com.caloriecounter.Utils;

import android.content.Context;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.Iterator;

/**
 * Created by Daniel on 28/08/2015.
 *

 This is the start code of the location service. Due to time limits, it is implemented but not used.
 The location of user are hard coded when registered.

 final LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
 Log.d("",locationManager.getAllProviders().toString());
 Criteria criteria = new Criteria();
 criteria.setAccuracy(Criteria.ACCURACY_FINE);
 String provider = locationManager.getBestProvider(criteria, false);
 LocationService locationService = new LocationService(MainActivity.this, locationManager);
 locationManager.addGpsStatusListener(locationService);

 locationManager.requestLocationUpdates(provider, 1000, 1, locationService);


//locationService.startLocationService();

*/
public class LocationService
        implements android.location.LocationListener,
        GpsStatus.Listener {

    private LocationManager locationManager;
    private Context mainActivity;
    public LocationService() {

    }

    public void onGpsStatusChanged(int event) {
        Log.d("","Gps status changed." + Integer.toString(event));
        if (event == GpsStatus.GPS_EVENT_SATELLITE_STATUS) {
            Iterator<GpsSatellite> itr = locationManager.getGpsStatus(null).getSatellites().iterator();
            /*
            while (itr.hasNext()) {
                GpsSatellite satellite = itr.next();
                Log.d("SatelitesInfo","PRN: " + Integer.toString(satellite.getPrn()) +
                        " SNR:" + Float.toString(satellite.getSnr()) +
                        " AZI:" + Float.toString(satellite.getAzimuth()) +
                        " ELE:" + Float.toString(satellite.getElevation()));
            }*/

        }
    }

    public LocationService(Context mainActivity, LocationManager locationManager) {
        this.locationManager = locationManager;
        this.mainActivity = mainActivity;
    }

    public void onStatusChanged(String provide, int status, Bundle extras) {

    }

    public void onProviderEnabled(String provider) {

    }

    public void onProviderDisabled(String provider) {

    }

    public void onLocationChanged(Location location) {

        Log.d("LocationService","Location change detected.");
        Toast.makeText(mainActivity, Double.toString(location.getLongitude()) + "\n" + Double.toString(location.getLatitude()), Toast.LENGTH_SHORT).show();
        Log.d("CurrentLocation"," Altitude:" + Double.toString(location.getAltitude()) +
                                " Accuracy:" + Float.toString(location.getAccuracy()) +
                                " Latitude:" + Double.toString(location.getLatitude()) +
                                " Longitude:" + Double.toString(location.getLongitude()));
    }



}
