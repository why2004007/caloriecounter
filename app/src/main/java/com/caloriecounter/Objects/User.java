package com.caloriecounter.Objects;

import com.caloriecounter.Utils.Encryption;

import java.util.Date;

/**
 * Created by Daniel on 28/08/2015.
 */
public class User {

    private int userId;
    private String username;
    private String nickname;
    private String password;
    private double height;
    private double weight;
    private int stepsPerMile;
    private Date regDate;
    private double latitude;
    private double longitude;
    private int activityLevel;
    private String gender;
    private Date dob;
    // Info for calculation
    private double dailyBurned = 0;
    private double goal = 0;
    private double bmr = 0;
    private double caloriePerStep = 0;
    private double totalBurned = 0;
    private double totalConsumed = 0;

    private static boolean isFullyInitialised = false;
    private static User instance;

    public User() {

    }

    public User(String username, String password) {
        this.username = username;
        this.password = Encryption.encryptPassword(password);
    }

    public static User getInstance() {
        return instance;
    }

    public static void setInstance(User instance) {
        User.instance = instance;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getActivityLevel() {
        return activityLevel;
    }

    public void setActivityLevel(int activityLevel) {
        this.activityLevel = activityLevel;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public int getUserId() {
        return userId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getStepsPerMile() {
        return stepsPerMile;
    }

    public void setStepsPerMile(int stepsPerMile) {
        this.stepsPerMile = stepsPerMile;
    }

    public double getDailyBurned() {
        return dailyBurned;
    }

    public void setDailyBurned(double dailyBurned) {
        this.dailyBurned = dailyBurned;
    }

    public double getGoal() {
        return goal;
    }

    public void setGoal(double goal) {
        this.goal = goal;
    }

    public double getBmr() {
        return bmr;
    }

    public void setBmr(double bmr) {
        this.bmr = bmr;
    }

    public double getCaloriePerStep() {
        return caloriePerStep;
    }

    public void setCaloriePerStep(double caloriePerStep) {
        this.caloriePerStep = caloriePerStep;
    }

    public double getTotalBurned() {
        return totalBurned;
    }

    public void setTotalBurned(double totalBurned) {
        this.totalBurned = totalBurned;
    }

    public double getTotalConsumed() {
        return totalConsumed;
    }

    public void setTotalConsumed(double totalConsumed) {
        this.totalConsumed = totalConsumed;
    }

    public static boolean isFullyInitialised() {
        return isFullyInitialised;
    }

    public static void setIsFullyInitialised(boolean isFullyInitialised) {
        User.isFullyInitialised = isFullyInitialised;
    }
}
