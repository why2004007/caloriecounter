package com.caloriecounter.Objects;

/**
 * Created by Daniel on 11/09/2015.
 */
public class Food {
    private String name;
    private String serveSize;
    private int foodId;

    public Food() {

    }

    public Food(int foodId, String name, String serveSize) {
        this.foodId = foodId;
        this.name = name;
        this.serveSize = serveSize;
    }

    public int getFoodId() {
        return foodId;
    }

    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServeSize() {
        return serveSize;
    }

    public void setServeSize(String serveSize) {
        this.serveSize = serveSize;
    }
}
